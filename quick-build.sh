# build angular frontend
(
  cd gui || exit
  ng build --localize
)

# clean any previous static files
rm -rf api/src/main/resources/static

# copy angular gui to spring boot's static folder
mkdir api/src/main/resources/static
cp -r gui/dist/gui/* api/src/main/resources/static/
