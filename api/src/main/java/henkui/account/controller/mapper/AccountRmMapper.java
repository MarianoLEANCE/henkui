package henkui.account.controller.mapper;

import henkui.account.Account;
import henkui.account.controller.links.AccountLinkCreator;
import henkui.account.controller.rm.AccountRm;
import henkui.transaction.controller.link.TransactionLinkCreator;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class AccountRmMapper {

    private final AccountLinkCreator accountLinkCreator;
    private final TransactionLinkCreator transactionLinkCreator;

    public AccountRm map(Account account) {
        AccountRm rm = new AccountRm();

        rm.setId(account.getId());
        rm.setDescription(account.getDescription());

        rm.add(accountLinkCreator.findLink(account).withSelfRel());
        rm.add(transactionLinkCreator.listLink(account).withRel("transactions"));

        return rm;
    }

    public CollectionModel<AccountRm> map(List<Account> accounts) {
        List<AccountRm> rms = accounts.stream().map(this::map).toList();
        return CollectionModel.of(rms, accountLinkCreator.listLink().withSelfRel());
    }

    public Account map(AccountRm rm) {
        Account monetaryAccount = new Account();

        monetaryAccount.setId(rm.getId());
        monetaryAccount.setDescription(rm.getDescription());

        return monetaryAccount;
    }

}
