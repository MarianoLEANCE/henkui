package henkui.account.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(collectionRelation = "accounts", itemRelation = "account")
public class AccountRm extends RepresentationModel<AccountRm> {

    /**
     * The monetary account's technical id.
     */
    Integer id;

    /**
     * A brief description of the account.
     */
    private String description;

}
