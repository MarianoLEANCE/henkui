package henkui.account.controller.links;

import henkui.account.Account;
import henkui.account.controller.AccountController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class AccountLinkCreator {

    public WebMvcLinkBuilder findLink(Account monetaryAccount) {
        return linkTo(methodOn(AccountController.class).find(monetaryAccount.getId()));
    }

    public WebMvcLinkBuilder listLink() {
        return linkTo(methodOn(AccountController.class).list());
    }

}
