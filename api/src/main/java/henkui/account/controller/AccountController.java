package henkui.account.controller;

import henkui.account.Account;
import henkui.account.controller.links.AccountLinkCreator;
import henkui.account.controller.mapper.AccountRmMapper;
import henkui.account.controller.rm.AccountRm;
import henkui.account.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Transactional
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/accounts")
public class AccountController {

    private final AccountService service;
    private final AccountRmMapper mapper;
    private final AccountLinkCreator linkCreator;

    // region create

    @PostMapping
    public ResponseEntity<?> create(@RequestBody AccountRm rm) {
        assertNew(rm);
        Account monetaryAccount = mapper.map(rm);
        Account persistant = service.create(monetaryAccount);
        return ResponseEntity.created(linkCreator.findLink(persistant).toUri()).build();
    }

    // endregion

    // region read

    @GetMapping("{account-id}")
    public AccountRm find(@PathVariable("account-id") Integer monetaryAccountId) {
        Account monetaryAccount = service.find(monetaryAccountId);
        return mapper.map(monetaryAccount);
    }

    @GetMapping
    public CollectionModel<AccountRm> list() {
        List<Account> monetaryAccounts = service.list();
        return mapper.map(monetaryAccounts);
    }

    // endregion

    // region update

    @PutMapping("{account-id}")
    public AccountRm update(@PathVariable("account-id") Integer monetaryAccountId,
                            @RequestBody AccountRm rm) {
        assertSame(monetaryAccountId, rm);
        Account persistant = service.find(monetaryAccountId);
        Account update = mapper.map(rm);
        service.update(persistant, update);
        return mapper.map(persistant);
    }

    // endregion

    // region delete

    @DeleteMapping("{account-id}")
    public void delete(@PathVariable("account-id") Integer accountId) {
        service.delete(accountId);
    }

    // endregion

    // region validation

    private void assertNew(AccountRm rm) {
        if (rm.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id should be null.");
        }
        assertValid(rm);
    }

    private void assertSame(Integer id, AccountRm rm) {
        if (!Objects.equals(id, rm.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id does not match.");
        }
        assertValid(rm);
    }

    private void assertValid(AccountRm rm) {
        if (rm.getDescription() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Description should not be null.");
        }
    }

    // endregion

}
