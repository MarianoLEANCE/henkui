package henkui.account.service;

import henkui.account.Account;
import henkui.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository repository;

    public Account create(Account monetaryAccount) {
        return repository.save(monetaryAccount);
    }

    public Account find(int accountId) {
        return repository.findById(accountId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Monetary account %d not found.".formatted(accountId)));
    }

    public List<Account> list() {
        List<Account> accounts = new ArrayList<>();
        repository.findAll().forEach(accounts::add);
        return accounts;
    }

    public Account update(Account persistant, Account update) {
        persistant.setDescription(update.getDescription());
        return repository.save(persistant);
    }

    public void delete(Integer monetaryAccountId) {
        repository.findById(monetaryAccountId).ifPresent(repository::delete);
    }

}
