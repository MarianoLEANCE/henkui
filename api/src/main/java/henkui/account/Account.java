package henkui.account;

import henkui.transaction.Transaction;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * An account holds money and has transactions.
 * It can be a bank account or a shoebox with cash.
 * In order to make meaningful data analysis, all transactions should be recorded.
 */
@Getter
@Setter
@Entity
@Table(name = "monetary_account")
public class Account {

    /**
     * The id of the transaction.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "monetary_account_id")
    private Integer id;

    /**
     * A brief description of the account.
     */
    @Column(name = "description")
    private String description;

    /**
     * All transactions of that account.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private List<Transaction> transactions;

}
