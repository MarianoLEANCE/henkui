package henkui.root;

import henkui.account.controller.AccountController;
import henkui.transaction.controller.TransactionController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
public class RootController {

    @GetMapping
    public ResponseEntity<RootRm> get() {
        RootRm resource = new RootRm();
        resource.add(linkTo(methodOn(RootController.class).get()).withSelfRel());
        resource.add(linkTo(methodOn(AccountController.class).list()).withRel("accounts"));
        resource.add(linkTo(methodOn(TransactionController.class).list(null)).withRel("transactions"));
        return ResponseEntity.ok(resource);
    }

}
