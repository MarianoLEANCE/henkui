//package henkui.api.importer.french_polynesia;
//
//import henkui.api.importer.CsvParser;
//import henkui.api.importer.ImporterUtils;
//import henkui.monetary_transaction.controller.rm.TransactionRm;
//import henkui.monetary_transaction.controller.mapper.TransactionRmMapper;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.transaction.Transactional;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Collection;
//import java.util.Optional;
//
//@Transactional
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/api/users/{userId}/accounts/{account-id}/import/french-polynesia/banque-socredo")
//public class BanqueSocredoCsvImporterController {
//
//    private final AccountService monetaryAccountService;
//    private final TransactionService monetaryTransactionService;
//    private final TransactionRmMapper mapper;
//    private final CsvParser csvImporter;
//    private final ImporterUtils importerUtils;
//
//    /**
//     * Import a csv from Banque Socredo to the account.
//     * The format consists of one record per line with the first line being the header description.
//     * The csv format is french localized with semicolons to separate values and uses LF as line separator.
//     * <code>Date comptable; Libellé opération; Débit; Crédit; Solde</code>
//     * The data is mapped as follows:<ol>
//     * <li>Date comptable -> accounting date with format dd/MM/yyyy</li>
//     * <li>Libellé opération -> description</li>
//     * <li>Libellé opération -> sometimes contains the value date minus the year</li>
//     * <li>Débit / Crédit -> amount</li>
//     * <li>Solde -> unmapped</li>
//     * </ol>
//     */
//    @PostMapping
//    public ResponseEntity<Collection<TransactionRm>> importData(
//            @RequestParam("file") MultipartFile file,
//            @PathVariable("userId") String userId,
//            @PathVariable("account-id") int accountId) throws IOException {
//
//        Optional<Account> account = monetaryAccountService.find(userId, accountId);
//        if (account.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//        DateTimeFormatter french = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//        String content = new String(file.getBytes(), StandardCharsets.ISO_8859_1);
//        return ResponseEntity.ok(csvImporter.records(content, ";", 1)
//                .map(values -> {
//                    Transaction monetaryTransaction = new Transaction();
//                    // 0 - Date comptable -> accounting date with format dd/MM/yyyy
//                    monetaryTransaction.setAccountingDate(LocalDate.parse(values[0], french));
//                    // 1 - Libelle -> description
//                    monetaryTransaction.setDescription(values[1].trim());
//                    // 2 / 3 - Débit / Crédit -> amount
//                    monetaryTransaction.setAmount(Double.parseDouble((values[2] + values[3]).replaceAll(",", ".")));
//                    // 4 - Solde -> unmapped
//
//                    monetaryTransaction.setMonetaryAccount(account.get());
//                    importerUtils.setDefaults(monetaryTransaction);
//                    return monetaryTransaction;
//                })
//                .map(monetaryTransactionService::create)
//                .map(mapper::map)
//                .toList());
//    }
//
//}
