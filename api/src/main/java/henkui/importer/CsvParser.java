package henkui.importer;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@Component
public class CsvParser {

    public Stream<List<String>> records(String file, String valueSeparator, int firstLine) {
        String normalized = normalizeLineEndings(file);
        List<String> lines = Arrays.asList(normalized.split("\n"));
        return lines.stream()
                .skip(firstLine)
                .filter(StringUtils::hasLength) // remove empty last line if exists
                .map(line -> split(line, valueSeparator));
    }

    public String normalizeLineEndings(String s) {
        return s.replaceAll("\r\n", "\n");
    }

    /**
     * Splits a record. The tricky part is to correctly interpret double-quoted values.
     */
    public List<String> split(String record, String separator) {
        // Captures a quoted value ending with a separator.
        Matcher quotedWithReminder = matcher("\"(.*?)\"" + separator + "(.*)", record);
        if (quotedWithReminder.matches()) {
            return prepend(quotedWithReminder.group(1), split(quotedWithReminder.group(2), separator));
        }

        // Captures the last quoted value.
        Matcher quoted = matcher("\"(.*)\"", record);
        if (quoted.matches()) {
            return List.of(quoted.group(1));
        }

        // Captures an unquoted value ending with a separator.
        Matcher unquotedWithReminder = matcher("(.*?)" + separator + "(.*)", record);
        if (unquotedWithReminder.matches()) {
            return prepend(unquotedWithReminder.group(1), split(unquotedWithReminder.group(2), separator));
        }

        // Captures the last unquoted value.
        return List.of(record);
    }

    private Matcher matcher(String regex, String s) {
        Pattern unquotedPatternWithReminder = Pattern.compile(regex);
        return unquotedPatternWithReminder.matcher(s);
    }

    private List<String> prepend(String s, List<String> l) {
        List<String> list = new ArrayList<>(l);
        list.add(0, s);
        return list;
    }

}
