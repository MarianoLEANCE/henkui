//package henkui.api.importer.generic;
//
//import henkui.api.importer.CsvParser;
//import henkui.api.importer.ImporterUtils;
//import henkui.monetary_transaction.controller.rm.TransactionRm;
//import henkui.monetary_transaction.controller.mapper.TransactionRmMapper;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.transaction.Transactional;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Collection;
//import java.util.Optional;
//
//@Transactional
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/api/users/{userId}/accounts/{account-id}/import/generic")
//public class GenericCsvImporterController {
//
//    private final AccountService monetaryAccountService;
//    private final TransactionService monetaryTransactionService;
//    private final TransactionRmMapper mapper;
//    private final CsvParser csvImporter;
//    private final ImporterUtils importerUtils;
//
//    /**
//     * The generic csv importer.
//     * The format consists of one record per line without headers.
//     * The csv format is the standard csv format with comas to separate values and uses LF as line separator.
//     * The data is mapped as follows:<ol>
//     * <li>accounting date with format yyyy-MM-dd</li>
//     * <li>value date with format yyyy-MM-dd</li>
//     * <li>description surrounded by double quotes</li>
//     * <li>amount</li>
//     * </ol>
//     */
//    @PostMapping
//    public ResponseEntity<Collection<TransactionRm>> importData(
//            @RequestParam("file") MultipartFile file,
//            @PathVariable("userId") String userId,
//            @PathVariable("account-id") int accountId) throws IOException {
//
//        Optional<Account> account = monetaryAccountService.find(userId, accountId);
//        if (account.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//        DateTimeFormatter iso = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        String content = new String(file.getBytes(), StandardCharsets.UTF_8);
//        return ResponseEntity.ok(csvImporter.records(content, ",", 0)
//                .map(values -> {
//                    Transaction monetaryTransaction = new Transaction();
//                    // 0 - accounting date with format yyyy-MM-dd
//                    monetaryTransaction.setAccountingDate(LocalDate.parse(values[0], iso));
//                    // 1 - description
//                    monetaryTransaction.setDescription(values[1]);
//                    // 2 - amount
//                    monetaryTransaction.setAmount(Double.parseDouble(values[2]));
//
//                    monetaryTransaction.setMonetaryAccount(account.get());
//                    importerUtils.setDefaults(monetaryTransaction);
//                    return monetaryTransaction;
//                })
//                .map(monetaryTransactionService::create)
//                .map(mapper::map)
//                .toList());
//    }
//
//}
