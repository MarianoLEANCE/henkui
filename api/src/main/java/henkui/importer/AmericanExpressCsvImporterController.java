package henkui.importer;

import henkui.account.Account;
import henkui.account.service.AccountService;
import henkui.transaction.Transaction;
import henkui.transaction.controller.mapper.TransactionRmMapper;
import henkui.transaction.controller.rm.TransactionRm;
import henkui.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * This importer will convert EUR to XPF using 119.3317 as conversion rate.
 * This importer will only work with XPF accounts.
 */
@Transactional
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/accounts/{account-id}/import/american-express")
public class AmericanExpressCsvImporterController {

    private final AccountService accountService;
    private final TransactionService transactionService;
    private final TransactionRmMapper mapper;
    private final CsvParser csvParser;

    /**
     * The format consists of one record per line with the first line being the header description.
     * The csv format is french localized with semicolons to separate values and uses LF as line separator.
     * <code>Date comptable;Libelle;Date valeur;Montant;Code operation</code>
     * The data is mapped as follows:<ol>
     * <li>Date -> accounting date</li>
     * <li>Description -> description</li>
     * <li>Montant -> amount</li>
     * </ol>
     */
    @PostMapping
    public List<TransactionRm> importData(@PathVariable("account-id") int accountId,
                                          @RequestParam("file") MultipartFile file) {
        Account account = accountService.find(accountId);
        String content = new String(bytes(file), StandardCharsets.ISO_8859_1);
        return csvParser.records(content, ",", 1)
                .map(values -> new Transaction(
                        account,
                        parseAmericanDate(values.get(0)),
                        parseAmountAndConvert(values.get(4)),
                        values.get(1)))
                .map(transactionService::create)
                .map(mapper::map)
                .toList();
    }

    private byte[] bytes(MultipartFile multipartFile) {
        try {
            return multipartFile.getBytes();
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to read file.");
        }
    }

    private LocalDate parseAmericanDate(String s) {
        return LocalDate.parse(s, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
    }

    private double parseAmountAndConvert(String s) {
        return Math.round(-1 * Float.parseFloat(s.replaceAll(",", ".")) * 119.3317);
    }

}
