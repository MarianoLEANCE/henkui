package henkui.importer;

import henkui.account.Account;
import henkui.account.service.AccountService;
import henkui.transaction.Transaction;
import henkui.transaction.controller.mapper.TransactionRmMapper;
import henkui.transaction.controller.rm.TransactionRm;
import henkui.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Transactional
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/accounts/{account-id}/import/banque-de-polynesie")
public class BanqueDePolynesieCsvImporterController {

    private final AccountService accountService;
    private final TransactionService transactionService;
    private final TransactionRmMapper mapper;
    private final CsvParser csvParser;

    /**
     * Import a csv from Banque de Polynésie to the account.
     * The format consists of one record per line with the first line being the header description.
     * The csv format is french localized with semicolons to separate values and uses CRLF as line separator.
     * <code>Date comptable;Libelle;Date valeur;Montant;Code operation</code>
     * The data is mapped as follows:<ol>
     * <li>Date comptable -> accounting date</li>
     * <li>Libelle -> description</li>
     * <li>Montant -> amount</li>
     * </ol>
     */
    @PostMapping
    public List<TransactionRm> importData(@PathVariable("account-id") int accountId,
                                          @RequestParam("file") MultipartFile file) throws IOException {
        Account account = accountService.find(accountId);
        String content = new String(file.getBytes(), StandardCharsets.ISO_8859_1);
        return csvParser.records(content, ";", 1)
                .map(values -> new Transaction(
                        account,
                        parseFrenchDate(values.get(0)),
                        Double.parseDouble(values.get(3)),
                        values.get(1)))
                .map(transactionService::create)
                .map(mapper::map)
                .toList();
    }

    private LocalDate parseFrenchDate(String s) {
        return LocalDate.parse(s, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

}
