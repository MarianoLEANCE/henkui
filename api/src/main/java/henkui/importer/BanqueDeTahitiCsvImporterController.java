//package henkui.api.importer.french_polynesia;
//
//import henkui.api.importer.CsvParser;
//import henkui.api.importer.ImporterUtils;
//import henkui.monetary_transaction.controller.rm.TransactionRm;
//import henkui.monetary_transaction.controller.mapper.TransactionRmMapper;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.transaction.Transactional;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.text.NumberFormat;
//import java.text.ParseException;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Collection;
//import java.util.Locale;
//import java.util.Optional;
//
//@Transactional
//@RestController
//@RequiredArgsConstructor
//@RequestMapping("/api/users/{userId}/accounts/{account-id}/import/french-polynesia/banque-de-tahiti")
//public class BanqueDeTahitiCsvImporterController {
//
//    private final AccountService monetaryAccountService;
//    private final TransactionService monetaryTransactionService;
//    private final TransactionRmMapper mapper;
//    private final CsvParser csvImporter;
//    private final ImporterUtils importerUtils;
//
//    /**
//     * Import a csv from Banque de Tahiti to the account.
//     * The format consists of one record per line with the first line being the header description.
//     * The csv format is french localized with semicolons to separate values and uses CRLF as line separator.
//     * <code>Numéro de compte;Intitulé de l'opération;Code devise;Devise;Montant;Date comptable;N° opération;
//     * Date de valeur;Numéro de chèque;Libellé d'opération;</code>
//     * The data is mapped as follows:<ol>
//     * <li>Numéro de compte -> unmapped, pathVariable account-id used instead</li>
//     * <li>Intitulé de l'opération -> unmapped, contains the holder's name which is not of use here</li>
//     * <li>Code devise -> unmapped, the currency is part of the account</li>
//     * <li>Devise -> unmapped, display name of the currency</li>
//     * <li>Montant -> amount</li>
//     * <li>Date comptable -> accounting date, format is dd/MM/yyyy</li>
//     * <li>N° opération -> unmapped</li>
//     * <li>Date de valeur -> value date, format is dd/MM/yyyy</li>
//     * <li>Numéro de chèque -> unmapped</li>
//     * <li>Libellé d'opération -> description</li>
//     * </ol>
//     */
//    @PostMapping
//    public ResponseEntity<Collection<TransactionRm>> importData(
//            @RequestParam("file") MultipartFile file,
//            @PathVariable("userId") String userId,
//            @PathVariable("account-id") int accountId) throws IOException {
//
//        Optional<Account> account = monetaryAccountService.find(userId, accountId);
//        if (account.isEmpty()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//
//        DateTimeFormatter french = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//        NumberFormat frenchNumberFormat = NumberFormat.getNumberInstance(Locale.FRANCE);
//        String content = new String(file.getBytes(), StandardCharsets.ISO_8859_1);
//        return ResponseEntity.ok(csvImporter.records(content, ";", 1)
//                .map(values -> {
//                    Transaction monetaryTransaction = new Transaction();
//
//                    // 0 - Numéro de compte -> unmapped, pathVariable account-id used instead
//                    // 1 - Intitulé de l'opération -> unmapped, contains the holder's name which is not of use here
//                    // 2 - Code devise -> unmapped, the currency is part of the account
//                    // 3 - Devise -> unmapped, display name of the currency
//                    // 4 - Montant -> amount
//                    try {
//                        monetaryTransaction.setAmount(frenchNumberFormat.parse(values[4]).longValue());
//                    } catch (ParseException e) {
//                        throw new RuntimeException("cannot parse amount", e);
//                    }
//                    // 5 - Date comptable -> accounting date, format is dd/MM/yyyy
//                    monetaryTransaction.setAccountingDate(LocalDate.parse(values[5], french));
//                    // 6 - N° opération -> unmapped
//                    // 7 - Date de valeur -> unmapped
//                    // 8 - Numéro de chèque -> unmapped
//                    // 9 - Libellé d'opération -> description
//                    monetaryTransaction.setDescription(values[9].trim());
//
//                    monetaryTransaction.setMonetaryAccount(account.get());
//                    importerUtils.setDefaults(monetaryTransaction);
//                    return monetaryTransaction;
//                })
//                .map(monetaryTransactionService::create)
//                .map(mapper::map)
//                .toList());
//    }
//
//}
