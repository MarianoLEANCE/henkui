package henkui.budget.service;

import henkui.budget.Budget;
import henkui.budget.repository.BudgetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BudgetService {

    private final BudgetRepository repository;

    public Budget create(Budget budget) {
        return repository.save(budget);
    }

    public Budget find(Integer budgetId) {
        return repository.findById(budgetId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                "Budget %d not found.".formatted(budgetId)));
    }

    public List<Budget> list() {
        return repository.findBudgetsByOrderByYearDesc();
    }

    public void update(Budget persistant, Budget update) {
        persistant.setYear(update.getYear());
        repository.save(persistant);
    }

    public void delete(Integer budgetId) {
        repository.findById(budgetId).ifPresent(repository::delete);
    }

}
