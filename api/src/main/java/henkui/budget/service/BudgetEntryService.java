package henkui.budget.service;

import henkui.budget.BudgetEntry;
import henkui.budget.SpendingRate;
import henkui.budget.repository.BudgetEntryRepository;
import henkui.transaction.Allocation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class BudgetEntryService {

    private final BudgetEntryRepository repository;

    // region create

    public BudgetEntry create(BudgetEntry budgetEntry) {
        assertSimilarNotExists(budgetEntry);
        return repository.save(budgetEntry);
    }

    private void assertSimilarNotExists(BudgetEntry budgetEntry) {
        if (repository.existsByBudget_IdAndCategoryAndSubcategory(budgetEntry.getBudget().getId(),
                budgetEntry.getCategory(), budgetEntry.getSubcategory())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "Budget %d already has an entry for %s/%s.".formatted(budgetEntry.getBudget().getId(),
                            budgetEntry.getCategory(), budgetEntry.getSubcategory()));
        }
    }

    // endregion

    // region read

    public BudgetEntry find(Integer budgetId, Integer budgetEntryId) {
        return repository.findByBudget_IdAndId(budgetId, budgetEntryId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Budget entry %d of budget %d not found.".formatted(budgetEntryId, budgetId)));
    }

    public BudgetEntry findByYearAndCategory(Integer budgetYear, String category, String subcategory) {
        return repository.findByBudget_YearAndCategoryAndSubcategory(budgetYear, category, subcategory)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Budget entry %s/%s of year %d not found.".formatted(category, subcategory, budgetYear)));
    }

    // endregion

    // region update

    public void update(BudgetEntry persistant, BudgetEntry update) {
        persistant.setAmount(update.getAmount());
        persistant.setCategory(update.getCategory());
        persistant.setSubcategory(update.getSubcategory());
        repository.save(persistant);
    }

    // endregion

    // region delete

    public void delete(Integer budgetId, Integer budgetEntryId) {
        repository.findByBudget_IdAndId(budgetId, budgetEntryId)
                .ifPresent(repository::delete);
    }

    // endregion

    public SpendingRate spendingRate(BudgetEntry budgetEntry) {
        List<SpendingRate.BudgetSpendingSnapshot> snapshots = daysOfYear(budgetEntry.getBudget().getYear()).stream()
                .map(date -> new SpendingRate.BudgetSpendingSnapshot(date,
                        spentUntilDate(date, budgetEntry),
                        limitToDate(date, budgetEntry),
                        budgetEntry.getAmount()))
                .toList();

        return new SpendingRate(snapshots, budgetEntry);
    }

    private List<LocalDate> daysOfYear(Integer year) {
        LocalDate start = LocalDate.of(year, Month.JANUARY, 1);
        LocalDate end = start.plusYears(1);
        return Stream.iterate(start, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(start, end))
                .collect(Collectors.toList());
    }

    private Double spentUntilDate(LocalDate date, BudgetEntry budgetEntry) {
        if (date.isAfter(LocalDate.now())) {
            return null;
        }
        return budgetEntry.getAllocations()
                .stream()
                .filter(entry -> !entry.getTransaction().getAccountingDate().isAfter(date))
                .mapToDouble(Allocation::getAmount)
                .sum();
    }

    private double limitToDate(LocalDate date, BudgetEntry entry) {
        return entry.getAmount() * ((double) date.getDayOfYear() / date.lengthOfYear());
    }

}
