package henkui.budget;

import henkui.transaction.Allocation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "budget_entry")
public class BudgetEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "budget_entry_id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "budget_id")
    private Budget budget;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "category")
    private String category;

    @Column(name = "subcategory")
    private String subcategory;

    @OneToMany(mappedBy = "budgetEntry")
    private List<Allocation> allocations;

}
