package henkui.budget;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * The budget contains the budget entries for a given accounting period.
 * In order to keep things simple, the budget will only represent a fiscal year.
 */
@Getter
@Setter
@Entity
@Table(name = "budget")
public class Budget {

    /**
     * The budget's technical ID.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "budget_id")
    private Integer id;

    /**
     * The fiscal year this budget is for.
     */
    @Column(name = "budget_year")
    private Integer year;

    /**
     * The entries of the budget.
     */
    @OneToMany(mappedBy = "budget")
    @OrderBy("category asc, subcategory asc")
    List<BudgetEntry> entries;

}
