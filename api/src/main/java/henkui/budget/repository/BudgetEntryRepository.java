package henkui.budget.repository;

import henkui.budget.BudgetEntry;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BudgetEntryRepository extends CrudRepository<BudgetEntry, Integer> {

    Optional<BudgetEntry> findByBudget_IdAndId(Integer budgetId, Integer budgetEntryId);

    Optional<BudgetEntry> findByBudget_YearAndCategoryAndSubcategory(Integer year, String category, String subcategory);

    Boolean existsByBudget_IdAndCategoryAndSubcategory(Integer budgetId, String category, String subcategory);

}
