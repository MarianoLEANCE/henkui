package henkui.budget.repository;

import henkui.budget.Budget;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BudgetRepository extends CrudRepository<Budget, Integer> {

    List<Budget> findBudgetsByOrderByYearDesc();

}
