package henkui.budget.controller.mapper;

import henkui.budget.Budget;
import henkui.budget.controller.links.BudgetEntryLinkCreator;
import henkui.budget.controller.links.BudgetLinkCreator;
import henkui.budget.controller.rm.BudgetRm;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BudgetRmMapper {

    private final BudgetLinkCreator budgetLinkCreator;
    private final BudgetEntryLinkCreator budgetEntryLinkCreator;

    public BudgetRm map(Budget budget) {
        BudgetRm rm = new BudgetRm();

        rm.setId(budget.getId());
        rm.setBudgetYear(budget.getYear());

        rm.add(budgetLinkCreator.findLink(budget).withSelfRel());
        rm.add(budgetLinkCreator.analyzeLink(budget).withRel("analyze"));
        rm.add(budgetEntryLinkCreator.listLink(budget).withRel("entries"));

        return rm;
    }

    public CollectionModel<BudgetRm> map(List<Budget> budgets) {
        List<BudgetRm> rms = budgets.stream().map(this::map).toList();
        return CollectionModel.of(rms, budgetLinkCreator.listLink().withSelfRel());
    }

    public Budget map(BudgetRm rm) {
        Budget budget = new Budget();

        budget.setId(rm.getId());
        budget.setYear(rm.getBudgetYear());

        return budget;
    }

}
