package henkui.budget.controller.mapper;

import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.budget.controller.links.BudgetEntryLinkCreator;
import henkui.budget.controller.rm.BudgetEntryRm;
import henkui.transaction.Allocation;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BudgetEntryRmMapper {

    private final BudgetEntryLinkCreator linkCreator;

    public BudgetEntryRm map(BudgetEntry budgetEntry) {
        BudgetEntryRm rm = new BudgetEntryRm();

        rm.setId(budgetEntry.getId());
        rm.setAmount(budgetEntry.getAmount());
        rm.setCategory(budgetEntry.getCategory());
        rm.setSubcategory(budgetEntry.getSubcategory());

        rm.setActual(budgetEntry.getAllocations().stream().mapToDouble(Allocation::getAmount).sum());

        rm.add(linkCreator.findLink(budgetEntry).withSelfRel());
        rm.add(linkCreator.spendingRate(budgetEntry).withRel("spendingRate"));

        return rm;
    }

    public CollectionModel<BudgetEntryRm> mapEntries(Budget budget) {
        List<BudgetEntry> budgetEntries = budget.getEntries();
        List<BudgetEntryRm> rms = budgetEntries.stream().map(this::map).toList();
        return CollectionModel.of(rms, linkCreator.listLink(budget).withSelfRel());
    }

    public BudgetEntry map(BudgetEntryRm rm) {
        BudgetEntry budgetEntry = new BudgetEntry();

        budgetEntry.setId(rm.getId());
        budgetEntry.setAmount(rm.getAmount());
        budgetEntry.setCategory(rm.getCategory());
        budgetEntry.setSubcategory(rm.getSubcategory());

        return budgetEntry;
    }

    public BudgetEntry map(BudgetEntryRm rm, Budget budget) {
        BudgetEntry budgetEntry = map(rm);

        budgetEntry.setBudget(budget);

        return budgetEntry;
    }

}
