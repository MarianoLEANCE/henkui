package henkui.budget.controller.mapper;

import henkui.budget.SpendingRate;
import henkui.budget.controller.BudgetEntryController;
import henkui.budget.controller.SpendingRateRm;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class SpendingRateRmMapper {

    public SpendingRateRm map(SpendingRate spendingRate) {
        SpendingRateRm rm = new SpendingRateRm();
        rm.setAmount(spendingRate.budgetEntry().getAmount());
        rm.setCategory(spendingRate.budgetEntry().getCategory());
        rm.setSubcategory(spendingRate.budgetEntry().getSubcategory());
        rm.setCumulativeExpenses(spendingRate.cumulativeExpenses().stream().map(this::map).toList());

        rm.add(linkTo(methodOn(BudgetEntryController.class)
                .spendingRate(spendingRate.budgetEntry().getBudget().getId(), spendingRate.budgetEntry().getId()))
                .withSelfRel());

        return rm;
    }

    SpendingRateRm.SpendingSnapshotRm map(SpendingRate.BudgetSpendingSnapshot snapshot) {
        SpendingRateRm.SpendingSnapshotRm rm = new SpendingRateRm.SpendingSnapshotRm();
        rm.setAccountingDate(snapshot.date());
        rm.setMaxPerYear(snapshot.maxPerYear());
        rm.setMaxPerDay(snapshot.maxPerDay());
        rm.setActualAmount(snapshot.actualAmount());
        return rm;
    }

}
