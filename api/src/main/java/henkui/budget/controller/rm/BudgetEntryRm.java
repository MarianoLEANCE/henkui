package henkui.budget.controller.rm;

import henkui.transaction.controller.rm.TransactionRm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(collectionRelation = "budgetEntries", itemRelation = "budgetEntry")
public class BudgetEntryRm extends RepresentationModel<TransactionRm> {

    /**
     * The budget's entry technical id.
     */
    Integer id;

    /**
     * How much money is allowed for this entry.
     */
    Double amount;

    /**
     * Actual earning/expense of the budget entry.
     */
    Double actual;

    /**
     * The category of the entry.
     */
    String category;

    /**
     * The subcategory of the entry.
     */
    String subcategory;

}
