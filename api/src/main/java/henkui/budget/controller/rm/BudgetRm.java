package henkui.budget.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(collectionRelation = "budgets", itemRelation = "budget")
public class BudgetRm extends RepresentationModel<BudgetRm> {

    /**
     * The budget's technical id.
     */
    Integer id;

    /**
     * The fiscal budget year.
     */
    Integer budgetYear;

}
