package henkui.budget.controller;

import henkui.budget.Budget;
import henkui.budget.controller.links.BudgetLinkCreator;
import henkui.budget.controller.mapper.BudgetRmMapper;
import henkui.budget.controller.rm.BudgetRm;
import henkui.budget.service.BudgetService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

@Transactional
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/budgets")
public class BudgetController {

    private final BudgetService service;
    private final BudgetRmMapper mapper;
    private final BudgetLinkCreator linkCreator;

    // region create

    @PostMapping
    public ResponseEntity<?> create(@RequestBody BudgetRm rm) {
        assertNew(rm);
        Budget newBudget = mapper.map(rm);
        Budget persistant = service.create(newBudget);
        return ResponseEntity.created(linkCreator.findLink(persistant).withSelfRel().toUri()).build();
    }

    // endregion

    // region read

    @GetMapping("{budget-id}")
    public BudgetRm find(@PathVariable("budget-id") Integer budgetId) {
        Budget budget = service.find(budgetId);
        return mapper.map(budget);
    }

    @GetMapping
    public CollectionModel<BudgetRm> list() {
        List<Budget> budgets = service.list();
        return mapper.map(budgets);
    }

    // endregion

    // region update

    @PutMapping("{budget-id}")
    public BudgetRm update(@PathVariable("budget-id") Integer budgetId, @RequestBody BudgetRm rm) {
        assertSame(budgetId, rm);
        Budget persistant = service.find(budgetId);
        Budget update = mapper.map(rm);
        service.update(persistant, update);
        return mapper.map(persistant);
    }

    // endregion

    // region delete

    @DeleteMapping("{budget-id}")
    public void delete(@PathVariable("budget-id") Integer budgetId) {
        service.delete(budgetId);
    }

    // endregion

    // region validation

    public void assertNew(BudgetRm rm) {
        if (rm.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id should be null.");
        }
        assertValid(rm);
    }

    private void assertSame(Integer id, BudgetRm rm) {
        if (!Objects.equals(id, rm.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id does not match.");
        }
        assertValid(rm);
    }

    private void assertValid(BudgetRm rm) {
        if (rm.getBudgetYear() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Year should not be null.");
        }
    }

    // endregion

}
