package henkui.budget.controller;

import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.budget.SpendingRate;
import henkui.budget.controller.links.BudgetEntryLinkCreator;
import henkui.budget.controller.mapper.BudgetEntryRmMapper;
import henkui.budget.controller.mapper.SpendingRateRmMapper;
import henkui.budget.controller.rm.BudgetEntryRm;
import henkui.budget.service.BudgetEntryService;
import henkui.budget.service.BudgetService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Objects;

@Transactional
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/budgets/{budget-id}/entries")
public class BudgetEntryController {

    private final BudgetService budgetService;
    private final BudgetEntryService budgetEntryService;
    private final BudgetEntryRmMapper budgetEntryRmMapper;
    private final SpendingRateRmMapper spendingRateRmMapper;
    private final BudgetEntryLinkCreator linkCreator;

    // region create

    @PostMapping
    public ResponseEntity<?> create(@PathVariable("budget-id") Integer budgetId,
                                    @RequestBody BudgetEntryRm rm) {
        assertNew(rm);
        Budget budget = budgetService.find(budgetId);
        BudgetEntry budgetEntry = budgetEntryRmMapper.map(rm, budget);
        BudgetEntry persistant = budgetEntryService.create(budgetEntry);
        return ResponseEntity.created(linkCreator.findLink(persistant).toUri()).build();
    }

    // endregion

    // region read

    @GetMapping("{budget-entry-id}")
    public BudgetEntryRm find(@PathVariable("budget-id") Integer budgetId,
                              @PathVariable("budget-entry-id") Integer budgetEntryId) {
        BudgetEntry budgetEntry = budgetEntryService.find(budgetId, budgetEntryId);
        return budgetEntryRmMapper.map(budgetEntry);
    }

    @GetMapping
    public CollectionModel<BudgetEntryRm> list(@PathVariable("budget-id") Integer budgetId) {
        Budget budget = budgetService.find(budgetId);
        return budgetEntryRmMapper.mapEntries(budget);
    }

    @GetMapping("{budget-entry-id}/spending-rate")
    public SpendingRateRm spendingRate(@PathVariable("budget-id") Integer budgetId,
                                       @PathVariable("budget-entry-id") int budgetEntryId) {
        BudgetEntry budgetEntry = budgetEntryService.find(budgetId, budgetEntryId);
        SpendingRate spendingRate = budgetEntryService.spendingRate(budgetEntry);
        return spendingRateRmMapper.map(spendingRate);
    }

    // endregion

    // region update

    @PutMapping("{budget-entry-id}")
    public BudgetEntryRm update(@PathVariable("budget-id") Integer budgetId,
                                @PathVariable("budget-entry-id") Integer budgetEntryId,
                                @RequestBody BudgetEntryRm rm) {
        assertSame(budgetEntryId, rm);
        BudgetEntry persistant = budgetEntryService.find(budgetId, budgetEntryId);
        BudgetEntry update = budgetEntryRmMapper.map(rm);
        budgetEntryService.update(persistant, update);
        return budgetEntryRmMapper.map(persistant);
    }

    // endregion

    // region delete

    @DeleteMapping("{budget-entry-id}")
    public void delete(@PathVariable("budget-id") Integer budgetId,
                       @PathVariable("budget-entry-id") Integer budgetEntryId) {
        Budget budget = budgetService.find(budgetId);
        budgetEntryService.delete(budget.getId(), budgetEntryId);
    }

    // endregion

    // region validation

    public void assertNew(BudgetEntryRm rm) {
        if (rm.getId() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id should be null.");
        }
        assertValid(rm);
    }

    private void assertSame(Integer id, BudgetEntryRm rm) {
        if (!Objects.equals(id, rm.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id does not match.");
        }
        assertValid(rm);
    }

    private void assertValid(BudgetEntryRm rm) {
        if (rm.getAmount() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Amount should not be null.");
        }
        if (rm.getCategory() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Category should not be null.");
        }
        if (rm.getSubcategory() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Subcategory should not be null.");
        }
    }

    // endregion

}
