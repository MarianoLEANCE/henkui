package henkui.budget.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class SpendingRateRm extends RepresentationModel<SpendingRateRm> {

    private List<SpendingSnapshotRm> cumulativeExpenses;
    private String category;
    private String subcategory;
    private Double amount;

    @Getter
    @Setter
    public static class SpendingSnapshotRm {

        private LocalDate accountingDate;
        private Double actualAmount;
        private Double maxPerYear;
        private Double maxPerDay;

    }

}
