package henkui.budget.controller.links;

import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.budget.controller.BudgetEntryController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BudgetEntryLinkCreator {

    public WebMvcLinkBuilder findLink(BudgetEntry budgetEntry) {
        return linkTo(methodOn(BudgetEntryController.class)
                .find(budgetEntry.getBudget().getId(), budgetEntry.getId()));
    }

    public WebMvcLinkBuilder spendingRate(BudgetEntry budgetEntry) {
        return linkTo(methodOn(BudgetEntryController.class)
                .spendingRate(budgetEntry.getBudget().getId(), budgetEntry.getId()));
    }

    public WebMvcLinkBuilder listLink(Budget budget) {
        return linkTo(methodOn(BudgetEntryController.class).list(budget.getId()));
    }

}
