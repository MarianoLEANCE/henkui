package henkui.budget.controller.links;

import henkui.budget.Budget;
import henkui.budget.controller.BudgetController;
import henkui.transaction.controller.TransactionController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class BudgetLinkCreator {

    public WebMvcLinkBuilder findLink(Budget budget) {
        return linkTo(methodOn(BudgetController.class).find(budget.getId()));
    }

    public WebMvcLinkBuilder listLink() {
        return linkTo(methodOn(BudgetController.class).list());
    }

    public WebMvcLinkBuilder analyzeLink(Budget budget) {
        return linkTo(methodOn(TransactionController.class).analyze(budget.getYear()));
    }

}
