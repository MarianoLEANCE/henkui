package henkui.budget;

import java.time.LocalDate;
import java.util.List;

public record SpendingRate(List<BudgetSpendingSnapshot> cumulativeExpenses,
                           BudgetEntry budgetEntry) {

    public record BudgetSpendingSnapshot(LocalDate date,
                                         Double actualAmount,
                                         Double maxPerDay,
                                         Double maxPerYear) {}

}
