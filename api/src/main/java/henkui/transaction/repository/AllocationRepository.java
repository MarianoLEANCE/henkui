package henkui.transaction.repository;

import henkui.transaction.Allocation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AllocationRepository extends CrudRepository<Allocation, Integer> {

    @Query("select allocation " +
           "from Allocation allocation " +
           "where allocation.transaction.id = :transactionId " +
           "  and allocation.budgetEntry.category = :category " +
           "  and allocation.budgetEntry.subcategory = :subcategory")
    Allocation findByTransactionAndCategory(Integer transactionId, String category, String subcategory);

}
