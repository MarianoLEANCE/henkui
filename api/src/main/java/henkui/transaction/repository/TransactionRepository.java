package henkui.transaction.repository;

import henkui.transaction.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends CrudRepository<Transaction, Integer> {

    Transaction getById(Integer id);

    Optional<Transaction> findByAccount_IdAndId(Integer accountId, Integer transactionId);

    List<Transaction> findByBudgetYearOrderByAccountingDateDesc(Integer budgetYear);

//    List<Transaction> findByYearAndCategory(String userId, int budgetYear, String category,
//                                                    String subcategory) {
//        return monetaryTransactionEntityRepository.findByYearAndCategory(userId, budgetYear, category, subcategory)
//                .stream()
//                .map(mapper::map).toList();
//
//    }

//    @Query("select distinct budgetYear from Transaction order by budgetYear")
//    List<Integer> years();

}
