package henkui.transaction;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.IntStream;

@Getter
@Setter
public class Analysis {

    private final List<MonthlyAnalysis> totalPerMonth;
    private Double totalAmount = 0d;

    public Analysis() {
        this.totalPerMonth = IntStream.rangeClosed(1, 12).boxed()
                .map(MonthlyAnalysis::new)
                .toList();
    }

    public void add(Integer month, Double amount) {
        this.totalAmount += amount;
        this.totalPerMonth.stream()
                .filter(monthly -> monthly.getMonth().equals(month))
                .findFirst().ifPresent(monthly -> monthly.add(amount));
    }

}
