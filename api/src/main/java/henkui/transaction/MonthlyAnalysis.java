package henkui.transaction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MonthlyAnalysis {

    private final Integer month;
    private Double totalAmount = 0d;

    public void add(Double amount) {
        this.totalAmount += amount;
    }

}
