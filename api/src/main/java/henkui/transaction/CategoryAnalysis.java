package henkui.transaction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class CategoryAnalysis extends Analysis implements Comparable<CategoryAnalysis> {

    private final String description;
    private final List<SubcategoryAnalysis> subcategories = new ArrayList<>();

    public void add(Integer month, String subcategory, Double amount) {
        super.add(month, amount);
        if (subcategories.stream().noneMatch(item -> item.getDescription().equals(subcategory))) {
            subcategories.add(new SubcategoryAnalysis(subcategory));
        }
        subcategories.stream().filter(categoryAnalysis -> categoryAnalysis.getDescription().equals(subcategory))
                .findFirst().ifPresent(categoryAnalysis -> categoryAnalysis.add(month, amount));
    }

    @Override
    public int compareTo(CategoryAnalysis o) {
        return description.compareTo(o.getDescription());
    }

}
