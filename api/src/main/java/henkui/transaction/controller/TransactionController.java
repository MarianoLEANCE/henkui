package henkui.transaction.controller;

import henkui.account.Account;
import henkui.account.service.AccountService;
import henkui.transaction.Transaction;
import henkui.transaction.TransactionAnalysis;
import henkui.transaction.controller.link.TransactionLinkCreator;
import henkui.transaction.controller.mapper.TransactionAnalysisMapper;
import henkui.transaction.controller.mapper.TransactionRmMapper;
import henkui.transaction.controller.rm.AllocationRm;
import henkui.transaction.controller.rm.DeallocationRm;
import henkui.transaction.controller.rm.TransactionAnalysisRm;
import henkui.transaction.controller.rm.TransactionRm;
import henkui.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Transactional
@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    private final AccountService accountService;
    private final TransactionRmMapper transactionRmMapper;
    private final TransactionAnalysisMapper transactionAnalysisMapper;
    private final TransactionLinkCreator linkCreator;

    // region create

    @PostMapping("/api/accounts/{account-id}/transactions")
    public ResponseEntity<?> create(@PathVariable("account-id") Integer accountId,
                                    @RequestBody TransactionRm rm) {
        assertNew(rm);
        Account monetaryAccount = accountService.find(accountId);
        Transaction transaction = transactionRmMapper.map(rm, monetaryAccount);
        Transaction persistant = transactionService.create(transaction);
        return ResponseEntity.created(linkCreator.findLink(persistant).toUri()).build();
    }

    // endregion

    // region read

    @GetMapping("/api/accounts/{account-id}/transactions/{transaction-id}")
    public TransactionRm find(@PathVariable("account-id") Integer accountId,
                              @PathVariable("transaction-id") Integer transactionId) {
        Transaction transaction = transactionService.find(accountId, transactionId);
        return transactionRmMapper.map(transaction);
    }

    @GetMapping("/api/accounts/{account-id}/transactions")
    public CollectionModel<TransactionRm> list(@PathVariable("account-id") Integer accountId) {
        Account monetaryAccount = accountService.find(accountId);
        return transactionRmMapper.mapTransactions(monetaryAccount);
    }

    /**
     * Gets all transactions regardless of the account.
     */
    @GetMapping("/api/transactions")
    public CollectionModel<TransactionRm> listAnyAccount(
            @RequestParam(value = "budget-year", required = false) Integer budgetYear) {
        List<TransactionRm> rms = transactionService.listForYear(budgetYear)
                .stream()
                .map(transactionRmMapper::map)
                .toList();
        return CollectionModel.of(rms,
                linkTo(methodOn(TransactionController.class).listAnyAccount(budgetYear)).withSelfRel());
    }

    @GetMapping("/api/transactions/analyze")
    public TransactionAnalysisRm analyze(@RequestParam(value = "budget-year") Integer budgetYear) {
        TransactionAnalysis transactionAnalysis = transactionService.analyze(budgetYear);
        TransactionAnalysisRm rm = transactionAnalysisMapper.map(transactionAnalysis);
        rm.add(linkTo(methodOn(TransactionController.class).analyze(budgetYear)).withSelfRel());
        return rm;
    }

    // endregion

    // region update

    @PutMapping("/api/accounts/{account-id}/transactions/{transaction-id}")
    public TransactionRm update(@PathVariable("account-id") Integer accountId,
                                @PathVariable("transaction-id") Integer transactionId,
                                @RequestBody TransactionRm rm) {
        assertSame(transactionId, rm);
        Transaction persistant = transactionService.find(accountId, transactionId);
        Transaction update = transactionRmMapper.map(rm);
        transactionService.update(persistant, update);
        return transactionRmMapper.map(persistant);
    }

    @PutMapping("/api/accounts/{account-id}/transactions/{transaction-id}/allocate")
    public TransactionRm allocate(@PathVariable("account-id") Integer accountId,
                                  @PathVariable("transaction-id") Integer transactionId,
                                  @RequestBody AllocationRm rm) {
        assertValid(rm);
        Transaction transaction = transactionService.find(accountId, transactionId);
        Transaction updated = transactionService.allocate(transaction, rm.getAmount(),
                rm.getCategory(), rm.getSubcategory());
        return transactionRmMapper.map(updated);
    }

    @PutMapping("/api/accounts/{account-id}/transactions/{transaction-id}/deallocate")
    public TransactionRm deallocate(@PathVariable("account-id") Integer accountId,
                                    @PathVariable("transaction-id") Integer transactionId,
                                    @RequestBody DeallocationRm rm) {
        assertValid(rm);
        Transaction transaction = transactionService.find(accountId, transactionId);
        Transaction updated = transactionService.deallocate(transaction, rm.getCategory(), rm.getSubcategory());
        return transactionRmMapper.map(updated);
    }

    // endregion

    // region delete

    @DeleteMapping("/api/accounts/{account-id}/transactions/{transaction-id}")
    public void delete(@PathVariable("account-id") Integer accountId,
                       @PathVariable("transaction-id") Integer transactionId) {
        Account monetaryAccount = accountService.find(accountId);
        transactionService.delete(monetaryAccount.getId(), transactionId);
    }

    // endregion

    // region validation

    public void assertNew(TransactionRm rm) {
        if (rm.getId() != null) {
            throw new ResponseStatusException(BAD_REQUEST, "Id should be null.");
        }
        assertValid(rm);
    }

    private void assertSame(Integer id, TransactionRm rm) {
        if (!Objects.equals(id, rm.getId())) {
            throw new ResponseStatusException(BAD_REQUEST, "Id does not match.");
        }
        assertValid(rm);
    }

    private void assertValid(TransactionRm rm) {
        if (rm.getDescription() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Description should not be null.");
        }
        if (rm.getAmount() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Amount should not be null.");
        }
        if (rm.getBudgetYear() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Budget year should not be null.");
        }
        if (rm.getBudgetMonth() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Budget month should not be null.");
        }
        if (rm.getAccountingDate() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Accounting date should not be null.");
        }
    }

    private void assertValid(AllocationRm rm) {
        if (rm.getAmount() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Amount should not be null.");
        }
        if (rm.getCategory() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Category should not be null.");
        }
        if (rm.getSubcategory() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Subcategory should not be null.");
        }
    }

    private void assertValid(DeallocationRm rm) {
        if (rm.getCategory() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Category should not be null.");
        }
        if (rm.getSubcategory() == null) {
            throw new ResponseStatusException(BAD_REQUEST, "Subcategory should not be null.");
        }
    }

    // endregion

}
