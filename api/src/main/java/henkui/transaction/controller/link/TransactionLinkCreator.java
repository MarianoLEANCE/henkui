package henkui.transaction.controller.link;

import henkui.account.Account;
import henkui.transaction.Transaction;
import henkui.transaction.controller.TransactionController;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TransactionLinkCreator {

    public WebMvcLinkBuilder findLink(Transaction transaction) {
        return linkTo(methodOn(TransactionController.class)
                .find(transaction.getAccount().getId(),
                        transaction.getId()));
    }

    public WebMvcLinkBuilder allocateLink(Transaction transaction) {
        return linkTo(methodOn(TransactionController.class)
                .allocate(transaction.getAccount().getId(), transaction.getId(), null));
    }

    public WebMvcLinkBuilder deallocateLink(Transaction transaction) {
        return linkTo(methodOn(TransactionController.class)
                .deallocate(transaction.getAccount().getId(), transaction.getId(), null));
    }

    public WebMvcLinkBuilder listLink(Account account) {
        return linkTo(methodOn(TransactionController.class).list(account.getId()));
    }

}
