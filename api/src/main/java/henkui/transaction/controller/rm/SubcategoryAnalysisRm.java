package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SubcategoryAnalysisRm {

    private String description;
    private Double totalAmount = 0d;
    private List<MonthlyAnalysisRm> totalPerMonth;

}
