package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Getter
@Setter
public class TransactionAnalysisRm extends RepresentationModel<TransactionAnalysisRm> {

    private Double totalAmount;
    private List<MonthlyAnalysisRm> totalPerMonth;
    private List<CategoryAnalysisRm> categories;

}
