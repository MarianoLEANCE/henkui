package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonthlyAnalysisRm {

    private Integer month;
    private Double totalAmount;

}
