package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Relation(collectionRelation = "transactions", itemRelation = "transaction")
public class TransactionRm extends RepresentationModel<TransactionRm> {

    /**
     * The monetary transaction's technical id.
     */
    private Integer id;

    /**
     * The day the transaction was registered.
     */
    private LocalDate accountingDate;

    /**
     * The year the user wants to set this transaction to show in the analysis.
     * This is useful when transactions get delayed to the next year.
     */
    private Integer budgetYear;

    /**
     * The month the user wants to set this transaction to show in the analysis.
     * This is useful when transactions get delayed to the next month.
     */
    private Integer budgetMonth;

    /**
     * How much money involves this transaction.
     */
    private Double amount;

    /**
     * A brief description.
     */
    private String description;

    /**
     * The account's id.
     */
    private Integer accountId;

    /**
     * How the transaction is allocated to the budget entries.
     */
    private List<AllocationRm> allocations;

}
