package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(collectionRelation = "allocation", itemRelation = "allocations")
public class AllocationRm extends RepresentationModel<AllocationRm> {

    /**
     * How much money is allowed allocated.
     */
    Double amount;

    /**
     * The category.
     */
    String category;

    /**
     * The subcategory.
     */
    String subcategory;

}
