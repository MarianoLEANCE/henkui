package henkui.transaction.controller.rm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

@Getter
@Setter
@Relation(collectionRelation = "allocation", itemRelation = "allocations")
public class DeallocationRm extends RepresentationModel<DeallocationRm> {

    /**
     * The category.
     */
    String category;

    /**
     * The subcategory.
     */
    String subcategory;

}
