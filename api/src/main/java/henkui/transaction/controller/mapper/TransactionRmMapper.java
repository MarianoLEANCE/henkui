package henkui.transaction.controller.mapper;

import henkui.account.Account;
import henkui.transaction.Allocation;
import henkui.transaction.Transaction;
import henkui.transaction.controller.link.TransactionLinkCreator;
import henkui.transaction.controller.rm.AllocationRm;
import henkui.transaction.controller.rm.TransactionRm;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class TransactionRmMapper {

    private final TransactionLinkCreator transactionLinkCreator;

    // region transaction to rm

    public TransactionRm map(Transaction transaction) {
        TransactionRm rm = new TransactionRm();

        rm.setId(transaction.getId());
        rm.setAccountingDate(transaction.getAccountingDate());
        rm.setBudgetMonth(transaction.getBudgetMonth());
        rm.setBudgetYear(transaction.getBudgetYear());
        rm.setAmount(transaction.getAmount());
        rm.setDescription(transaction.getDescription());

        rm.setAccountId(transaction.getAccount().getId());

        rm.setAllocations(transaction.getAllocations().stream().map(this::map).toList());

        rm.add(transactionLinkCreator.findLink(transaction).withSelfRel());
        rm.add(transactionLinkCreator.allocateLink(transaction).withRel("allocate"));
        rm.add(transactionLinkCreator.deallocateLink(transaction).withRel("deallocate"));

        return rm;
    }

    private AllocationRm map(Allocation allocation) {
        AllocationRm rm = new AllocationRm();
        rm.setAmount(allocation.getAmount());
        rm.setCategory(allocation.getBudgetEntry().getCategory());
        rm.setSubcategory(allocation.getBudgetEntry().getSubcategory());
        return rm;
    }

    public CollectionModel<TransactionRm> mapTransactions(Account monetaryAccount) {
        List<Transaction> transactions = monetaryAccount.getTransactions();
        List<TransactionRm> rms = transactions.stream().map(this::map).toList();
        return CollectionModel.of(rms, transactionLinkCreator.listLink(monetaryAccount).withSelfRel());
    }

    // endregion

    // region rm to transaction

    public Transaction map(TransactionRm rm, Account monetaryAccount) {
        Transaction transaction = map(rm);

        transaction.setAccount(monetaryAccount);

        return transaction;
    }

    public Transaction map(TransactionRm rm) {
        Transaction transaction = new Transaction();

        transaction.setAccountingDate(rm.getAccountingDate());
        transaction.setBudgetMonth(rm.getBudgetMonth());
        transaction.setBudgetYear(rm.getBudgetYear());
        transaction.setAmount(rm.getAmount());
        transaction.setDescription(rm.getDescription());

        return transaction;
    }

    // endregion

}
