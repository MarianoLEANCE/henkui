package henkui.transaction.controller.mapper;

import henkui.transaction.CategoryAnalysis;
import henkui.transaction.MonthlyAnalysis;
import henkui.transaction.SubcategoryAnalysis;
import henkui.transaction.TransactionAnalysis;
import henkui.transaction.controller.rm.CategoryAnalysisRm;
import henkui.transaction.controller.rm.MonthlyAnalysisRm;
import henkui.transaction.controller.rm.SubcategoryAnalysisRm;
import henkui.transaction.controller.rm.TransactionAnalysisRm;
import org.springframework.stereotype.Component;

@Component
public class TransactionAnalysisMapper {

    public TransactionAnalysisRm map(TransactionAnalysis analysis) {
        TransactionAnalysisRm rm = new TransactionAnalysisRm();
        rm.setTotalAmount(analysis.getTotalAmount());
        rm.setTotalPerMonth(analysis.getTotalPerMonth().stream().map(this::map).toList());
        rm.setCategories(analysis.getCategories().stream().map(this::map).toList());
        return rm;
    }

    public CategoryAnalysisRm map(CategoryAnalysis analysis) {
        CategoryAnalysisRm rm = new CategoryAnalysisRm();
        rm.setDescription(analysis.getDescription());
        rm.setTotalAmount(analysis.getTotalAmount());
        rm.setTotalPerMonth(analysis.getTotalPerMonth().stream().map(this::map).toList());
        rm.setSubcategories(analysis.getSubcategories().stream().map(this::map).toList());
        return rm;
    }

    public SubcategoryAnalysisRm map(SubcategoryAnalysis analysis) {
        SubcategoryAnalysisRm rm = new SubcategoryAnalysisRm();
        rm.setDescription(analysis.getDescription());
        rm.setTotalAmount(analysis.getTotalAmount());
        rm.setTotalPerMonth(analysis.getTotalPerMonth().stream().map(this::map).toList());
        return rm;
    }

    private MonthlyAnalysisRm map(MonthlyAnalysis analysis) {
        MonthlyAnalysisRm rm = new MonthlyAnalysisRm();
        rm.setMonth(analysis.getMonth());
        rm.setTotalAmount(analysis.getTotalAmount());
        return rm;
    }

}
