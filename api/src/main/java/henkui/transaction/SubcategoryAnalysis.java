package henkui.transaction;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SubcategoryAnalysis extends Analysis implements Comparable<SubcategoryAnalysis> {

    private final String description;

    @Override
    public int compareTo(SubcategoryAnalysis o) {
        return description.compareTo(o.description);
    }

}
