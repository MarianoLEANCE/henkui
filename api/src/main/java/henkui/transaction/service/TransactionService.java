package henkui.transaction.service;

import henkui.budget.BudgetEntry;
import henkui.budget.service.BudgetEntryService;
import henkui.transaction.Allocation;
import henkui.transaction.Transaction;
import henkui.transaction.TransactionAnalysis;
import henkui.transaction.repository.AllocationRepository;
import henkui.transaction.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static java.util.Optional.ofNullable;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final EntityManager entityManager;

    private final AllocationRepository allocationRepository;
    private final BudgetEntryService budgetEntryService;
    private final TransactionRepository transactionRepository;

    public Transaction create(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public Transaction find(Integer monetaryAccountId, Integer monetaryTransactionId) {
        return transactionRepository.findByAccount_IdAndId(monetaryAccountId,
                        monetaryTransactionId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        "Transaction %d of account %d not found.".formatted(monetaryAccountId, monetaryTransactionId)));
    }

    public List<Transaction> listForYear(Integer budgetYear) {
        return transactionRepository.findByBudgetYearOrderByAccountingDateDesc(budgetYear);
    }

    public TransactionAnalysis analyze(Integer budgetYear) {
        TransactionAnalysis transactionAnalysis = new TransactionAnalysis();
        List<Transaction> transactions = listForYear(budgetYear);
        transactions.stream()
                .map(Transaction::getAllocations)
                .flatMap(List::stream)
                .forEach(allocation -> {
                    String category = allocation.getBudgetEntry().getCategory();
                    String subcategory = allocation.getBudgetEntry().getSubcategory();
                    Integer month = allocation.getTransaction().getBudgetMonth();
                    Double amount = allocation.getAmount();
                    transactionAnalysis.add(category, subcategory, month, amount);
                });
        Collections.sort(transactionAnalysis.getCategories());
        transactionAnalysis.getCategories().forEach(subcat -> Collections.sort(subcat.getSubcategories()));
        return transactionAnalysis;
    }

    public Transaction update(Transaction persistant, Transaction update) {
        persistant.setAccountingDate(update.getAccountingDate());
        persistant.setBudgetYear(update.getBudgetYear());
        persistant.setBudgetMonth(update.getBudgetMonth());
        persistant.setAmount(update.getAmount());
        persistant.setDescription(update.getDescription());
        return transactionRepository.save(persistant);
    }

    public Transaction allocate(Transaction transaction, Double amount, String category, String subcategory) {
        ofNullable(allocationRepository.findByTransactionAndCategory(transaction.getId(), category, subcategory))
                .ifPresent(allocationRepository::delete);
        BudgetEntry budgetEntry = budgetEntryService
                .findByYearAndCategory(transaction.getBudgetYear(), category, subcategory);
        Allocation allocation = new Allocation();
        allocation.setAmount(amount);
        allocation.setTransaction(transaction);
        allocation.setBudgetEntry(budgetEntry);
        allocationRepository.save(allocation);
        entityManager.flush();
        entityManager.clear(); // The transaction has already been fetched but has changed.
        return transactionRepository.getById(transaction.getId());
    }

    public Transaction deallocate(Transaction transaction, String category, String subcategory) {
        ofNullable(allocationRepository.findByTransactionAndCategory(transaction.getId(), category, subcategory))
                .ifPresent(allocationRepository::delete);
        entityManager.flush();
        entityManager.clear(); // The transaction has already been fetched but has changed.
        return transactionRepository.getById(transaction.getId());
    }

    public void delete(Integer monetaryAccountId, Integer monetaryTransactionId) {
        transactionRepository.findByAccount_IdAndId(monetaryAccountId,
                        monetaryTransactionId)
                .ifPresent(transactionRepository::delete);
    }

}
