package henkui.transaction;

import henkui.budget.BudgetEntry;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * An Allocation represents how much money is allocated to a particular budget entry for a given transaction.
 * Most of the time, 100% of the transaction is allocated to a single budget transaction.
 * However, in some cases we may want to split the expense.
 */
@Getter
@Setter
@Entity
@Table(name = "allocation")
public class Allocation {

    /**
     * The id of the allocation.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "allocation_id")
    private Integer allocationId;

    @ManyToOne
    @JoinColumn(name = "monetary_transaction_id")
    private Transaction transaction;

    @ManyToOne
    @JoinColumn(name = "budget_entry_id")
    private BudgetEntry budgetEntry;

    @Column(name = "amount")
    private Double amount;

}
