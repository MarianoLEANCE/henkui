package henkui.transaction;

import henkui.account.Account;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "monetary_transaction")
public class Transaction {

    /**
     * The id of the transaction.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "monetary_transaction_id")
    private Integer id;

    /**
     * The account owning the transaction.
     */
    @ManyToOne
    @JoinColumn(name = "monetary_account_id")
    private Account account;

    /**
     * The day the transaction was registered.
     */
    @Column(name = "accounting_date")
    private LocalDate accountingDate;

    /**
     * The year the user wants to set this transaction to show in the analysis.
     * This is useful when transactions get delayed to the next year.
     */
    @Column(name = "budget_year")
    private Integer budgetYear;

    /**
     * The month the user wants to set this transaction to show in the analysis.
     * This is useful when transactions get delayed to the next month.
     */
    @Column(name = "budget_month")
    private Integer budgetMonth;

    /**
     * How much money involves this transaction.
     */
    @Column(name = "amount")
    private Double amount;

    /**
     * A brief description.
     */
    @Column(name = "description")
    private String description;

    /**
     * How the transaction is allocated to the budget entries.
     */
    @OneToMany(mappedBy = "transaction", cascade = CascadeType.REMOVE)
    private List<Allocation> allocations;

    public Transaction() {}

    public Transaction(Account account, LocalDate accountingDate, Double amount, String description) {
        this.account = account;
        this.accountingDate = accountingDate;
        this.budgetYear = accountingDate.getYear();
        this.budgetMonth = accountingDate.getMonthValue();
        this.amount = amount;
        this.description = description;
        this.allocations = new ArrayList<>();
    }

}
