package henkui.transaction;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class TransactionAnalysis extends Analysis {

    private List<CategoryAnalysis> categories = new ArrayList<>();

    public void add(String category, String subcategory, Integer month, Double amount) {
        super.add(month, amount);
        if (categories.stream().noneMatch(item -> item.getDescription().equals(category))) {
            categories.add(new CategoryAnalysis(category));
        }
        categories.stream().filter(categoryAnalysis -> categoryAnalysis.getDescription().equals(category))
                .forEach(categoryAnalysis -> categoryAnalysis.add(month, subcategory, amount));
    }

}
