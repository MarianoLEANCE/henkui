package henkui.account.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import henkui.HenkuiIntegrationTest;
import henkui.account.Account;
import henkui.account.controller.rm.AccountRm;
import henkui.account.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class AccountControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    AccountService service;
    @Autowired
    ObjectMapper objectMapper;

    private Account createMonetaryAccount() {
        Account budget = new Account();
        budget.setDescription("checking account");
        return service.create(budget);
    }

    @Test
    @WithMockUser
    void create() throws Exception {
        AccountRm rm = new AccountRm();
        rm.setDescription("checking account");
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    void create_withId() throws Exception {
        AccountRm rm = new AccountRm();
        rm.setId(1); // id should be null
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_invalid() throws Exception {
        AccountRm rm = new AccountRm(); // no description
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void find() throws Exception {
        Account monetaryAccount = createMonetaryAccount();
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/" + monetaryAccount.getId()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void find_notFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void list() throws Exception {
        Account monetaryAccount = new Account();
        monetaryAccount.setDescription("checking account");
        service.create(monetaryAccount);
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.accounts.size()", equalTo(1)))
                .andExpect(jsonPath("$._embedded.accounts[0].description",
                        equalTo("checking account")));
    }

    @Test
    @WithMockUser
    void update() throws Exception {
        Account monetaryAccount = createMonetaryAccount();
        AccountRm rm = new AccountRm();
        rm.setDescription("savings account");
        rm.setId(monetaryAccount.getId());
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d".formatted(monetaryAccount.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description", equalTo(rm.getDescription())));
    }

    @Test
    @WithMockUser
    void update_BadId() throws Exception {
        Account monetaryAccount = createMonetaryAccount();
        AccountRm rm = new AccountRm(); // no id
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d".formatted(monetaryAccount.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void delete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/accounts/0"))
                .andExpect(status().isOk());
    }

}
