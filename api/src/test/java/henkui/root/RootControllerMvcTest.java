package henkui.root;

import henkui.HenkuiIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class RootControllerMvcTest {

    @Autowired
    MockMvc mvc;

    @Test
    @WithMockUser
    void get() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api")).andExpect(status().isOk());
    }

}
