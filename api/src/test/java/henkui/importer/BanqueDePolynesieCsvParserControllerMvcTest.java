package henkui.importer;

import henkui.HenkuiIntegrationTest;
import henkui.IntegrationTestUtils;
import henkui.account.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class BanqueDePolynesieCsvParserControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    IntegrationTestUtils utils;

    public static final String data = """
            Date comptable;Libelle;Date valeur;Montant;Code operation
            30/12/2021;FRAIS GESTION PRELEVEMENT 2021;2021-12-30;-1200;DIV
            29/12/2021;ECB2612PDG ST SHELL 36,22EUR;2021-12-29;-4322;ECB
            27/12/2021;ECB2412MR BRICOLAGE 9,23EUR;2021-12-27;-1101;ECB
            07/12/2021;ECB0512MARINA EXPRE 51,55EUR;2021-12-07;-6152;ECB
            """;

    @Test
    @WithMockUser
    void importData() throws Exception {
        Account account = utils.create(utils.account());
        mvc.perform(multipart("/api/accounts/%d/import/banque-de-polynesie".formatted(account.getId()))
                        .file("file", data.getBytes(StandardCharsets.ISO_8859_1)))
                .andExpect(status().isOk());
    }

}
