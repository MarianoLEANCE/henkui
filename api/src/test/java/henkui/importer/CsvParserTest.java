package henkui.importer;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CsvParserTest {

    CsvParser csvParser = new CsvParser();

    @Test
    void records() {
        String csv = """
                first name,last name,age
                Bob,Sponge,2
                Patrick,Star,2
                """;
        List<List<String>> records = csvParser.records(csv, ",", 1).toList();
        assertEquals(2, records.size());
        assertTrue(records.contains(List.of("Bob", "Sponge", "2")));
        assertTrue(records.contains(List.of("Patrick", "Star", "2")));
    }

    @Test
    void normalizeLineEndings() {
        String lines = "foo\r\nbar\r\n";
        String normalized = csvParser.normalizeLineEndings(lines);
        assertEquals("foo\nbar\n", normalized);
    }

    @Test
    void split() {
        List<String> tokens = csvParser.split("a,b,c", ",");
        assertEquals(List.of("a", "b", "c"), tokens);
    }

    @Test
    void split_endsWithSeparator() {
        List<String> tokens = csvParser.split("a,b,c,", ",");
        assertEquals(List.of("a", "b", "c", ""), tokens);
    }

    @Test
    void split_withEmpty() {
        List<String> tokens = csvParser.split("a,,b", ",");
        assertEquals(List.of("a", "", "b"), tokens);
    }

    @Test
    void split_quoted() {
        List<String> tokens = csvParser.split("a,\"b,c\",d,\"e,f\"", ",");
        assertEquals(List.of("a", "b,c", "d", "e,f"), tokens);
    }

}
