package henkui.importer;

import henkui.HenkuiIntegrationTest;
import henkui.IntegrationTestUtils;
import henkui.account.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class AmericanExpressCsvParserControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    IntegrationTestUtils utils;

    public static String data = """
            Date,Description,Titulaire,Numéro de Compte,Montant
            10/22/2021,APPLE.COM/BILL          HOLLYHILL,BOB SPONGE,-12345,"2,99"
            10/22/2021,SHELL RDO               PACIFIC DEVEL,BOB SPONGE,-12345,"46,09"
            10/18/2021,CARREFOUR PUNAAUIA      CARREFOUR PUN,BOB SPONGE,-12345,"200,13"
            10/16/2021,MAG  WEEK END           MAGASIN WEEK,BOB SPONGE,-12345,"41,57"
            10/16/2021,STATION TOTAL TIPAE     TOTAL TIPAERU,BOB SPONGE,-12345,"40,98"
            10/15/2021,STATION TOTAL TIPAE     TOTAL TIPAERU,BOB SPONGE,-12345,"55,90"
            10/14/2021,DR.HOUSE                DR HOUSE     ,BOB SPONGE,-12345,"30,17"
            10/13/2021,CARREFOUR               CARREFOUR FAA,BOB SPONGE,-12345,"25,77"
            10/02/2021,NETFLIX.COM 123456      SINGAPORE,BOB SPONGE,-12345,"8,87"
            10/02/2021,SUPERMARCHE MARINA      SUPERMARCHE M,BOB SPONGE,-12345,"50,68"
            09/30/2021,APPLE.COM/BILL          HOLLYHILL,BOB SPONGE,-12345,"14,99"
            09/30/2021,SHELL RDO               PACIFIC DEVEL,BOB SPONGE,-12345,"48,61"
            09/29/2021,PRELEVEMENT AUTOMATIQUE ENREGISTRE-MERCI,BOB SPONGE,-12345,"-1200,00"
            """;

    @Test
    @WithMockUser
    void importData() throws Exception {
        Account account = utils.create(utils.account());
        mvc.perform(multipart("/api/accounts/%d/import/american-express".formatted(account.getId()))
                        .file("file", data.getBytes(StandardCharsets.ISO_8859_1)))
                .andExpect(status().isOk());
    }

}
