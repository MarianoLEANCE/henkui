package henkui.transaction.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import henkui.HenkuiIntegrationTest;
import henkui.IntegrationTestUtils;
import henkui.account.Account;
import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.transaction.Transaction;
import henkui.transaction.controller.rm.AllocationRm;
import henkui.transaction.controller.rm.DeallocationRm;
import henkui.transaction.controller.rm.TransactionRm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityManager;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class TransactionControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    EntityManager entityManager;
    @Autowired
    IntegrationTestUtils utils;

    @Test
    @WithMockUser
    void create() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    void create_withId() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setId(1);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noDescription() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setDescription(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noAmount() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setAmount(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noBudgetYear() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setBudgetYear(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noBudgetMonth() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setBudgetMonth(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noAccountingDate() throws Exception {
        Account account = utils.create(utils.account());
        TransactionRm rm = utils.rm(utils.transaction(account));
        rm.setAccountingDate(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/accounts/%d/transactions"
                                .formatted(account.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void find() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/%d/transactions/%d"
                        .formatted(transaction.getAccount().getId(),
                                transaction.getId())))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void find_notFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/0/transactions/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void list() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.get("/api/accounts/%d/transactions"
                        .formatted(transaction.getAccount().getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.transactions.size()", equalTo(1)))
                .andExpect(jsonPath("$._embedded.transactions[0].budgetYear",
                        equalTo(transaction.getBudgetYear())))
                .andExpect(jsonPath("$._embedded.transactions[0].budgetMonth",
                        equalTo(transaction.getBudgetMonth())))
                .andExpect(jsonPath("$._embedded.transactions[0].amount",
                        comparesEqualTo(transaction.getAmount())))
                .andExpect(jsonPath("$._embedded.transactions[0].accountingDate",
                        equalTo(transaction.getAccountingDate().format(DateTimeFormatter.ISO_DATE))))
                .andExpect(jsonPath("$._embedded.transactions[0].description",
                        equalTo(transaction.getDescription())));
    }

    @Test
    @WithMockUser
    void listAnyAccount() throws Exception {
        Account account1 = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account1));
        Account account2 = utils.create(utils.account());
        utils.create(utils.transaction(account2));
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.get("/api/transactions")
                        .param("budget-year", transaction.getBudgetYear().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.transactions.size()", equalTo(2)));
    }

    @Test
    @WithMockUser
    void analyze() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        Account account1 = utils.create(utils.account());
        Transaction transaction1 = utils.create(utils.transaction(account1));
        utils.create(utils.allocation(transaction1, budgetEntry));
        Account account2 = utils.create(utils.account());
        Transaction transaction2 = utils.create(utils.transaction(account2));
        utils.create(utils.allocation(transaction2, budgetEntry));
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.get("/api/transactions/analyze")
                        .param("budget-year", transaction1.getBudgetYear().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.totalAmount", comparesEqualTo(-6d)))
                .andExpect(jsonPath("$.totalPerMonth[7].totalAmount", comparesEqualTo(-6d)))
                .andExpect(jsonPath("$.categories[0].totalAmount", comparesEqualTo(-6d)))
                .andExpect(jsonPath("$.categories[0].subcategories[0].totalAmount",
                        comparesEqualTo(-6d)));
    }

    @Test
    @WithMockUser
    void update() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        TransactionRm rm = utils.rm(transaction);
        rm.setDescription("tagliatelle");
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d/transactions/%d"
                                .formatted(transaction.getAccount().getId(),
                                        transaction.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.description", equalTo(rm.getDescription())));
    }

    @Test
    @WithMockUser
    void allocate() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        AllocationRm rm = new AllocationRm();
        rm.setAmount(transaction.getAmount());
        rm.setCategory(budgetEntry.getCategory());
        rm.setSubcategory(budgetEntry.getSubcategory());
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d/transactions/%d/allocate"
                                .formatted(transaction.getAccount().getId(),
                                        transaction.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.allocations[0].amount", equalTo(rm.getAmount())))
                .andExpect(jsonPath("$.allocations[0].category", equalTo(rm.getCategory())))
                .andExpect(jsonPath("$.allocations[0].subcategory", equalTo(rm.getSubcategory())));
    }

    @Test
    @WithMockUser
    void allocate_budgetEntryNotFound() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        AllocationRm rm = new AllocationRm();
        rm.setAmount(transaction.getAmount());
        rm.setCategory("foo");
        rm.setSubcategory("bar");
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d/transactions/%d/allocate"
                                .formatted(transaction.getAccount().getId(),
                                        transaction.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void allocate_noAmount() throws Exception {
        AllocationRm rm = new AllocationRm();
        rm.setAmount(null);
        rm.setCategory("category");
        rm.setSubcategory("subcategory");
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/0/transactions/0/allocate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void allocate_noCategory() throws Exception {
        AllocationRm rm = new AllocationRm();
        rm.setAmount(1d);
        rm.setCategory(null);
        rm.setSubcategory("subcategory");
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/0/transactions/0/allocate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void allocate_noSubcategory() throws Exception {
        AllocationRm rm = new AllocationRm();
        rm.setAmount(1d);
        rm.setCategory("category");
        rm.setSubcategory(null);
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/0/transactions/0/allocate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void deallocate() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        utils.create(utils.allocation(transaction, budgetEntry));
        entityManager.flush();
        entityManager.clear();
        DeallocationRm rm = new DeallocationRm();
        rm.setCategory(budgetEntry.getCategory());
        rm.setSubcategory(budgetEntry.getSubcategory());
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d/transactions/%d/deallocate"
                                .formatted(transaction.getAccount().getId(),
                                        transaction.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.allocations.size()", equalTo(0)));
    }

    @Test
    @WithMockUser
    void deallocate_noCategory() throws Exception {
        DeallocationRm rm = new DeallocationRm();
        rm.setCategory(null);
        rm.setSubcategory("subcategory");
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/0/transactions/0/deallocate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void deallocate_noSubcategory() throws Exception {
        DeallocationRm rm = new DeallocationRm();
        rm.setCategory("category");
        rm.setSubcategory(null);
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/0/transactions/0/deallocate")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void update_BadId() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        TransactionRm rm = utils.rm(transaction);
        rm.setId(-1);
        mvc.perform(MockMvcRequestBuilders.put("/api/accounts/%d/transactions/%d"
                                .formatted(transaction.getAccount().getId(),
                                        transaction.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void delete() throws Exception {
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        mvc.perform(MockMvcRequestBuilders.delete("/api/accounts/%d/transactions/%d"
                        .formatted(account.getId(), transaction.getId())))
                .andExpect(status().isOk());
    }

}
