//package henkui.api.importer.generic;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.domain.user_account.UserAccount;
//import henkui.domain.user_account.UserAccountService;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.nio.charset.StandardCharsets;
//
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@HenkuiIntegrationTest
//public class GenericCsvImporterMvcTest {
//
//    private final String userId = "123";
//    @Autowired
//    UserAccountService userService;
//    @Autowired
//    AccountService monetaryAccountService;
//
//    @Autowired
//    MockMvc mvc;
//
//
//    @Test
//    @WithMockUser(username = userId)
//    void importCsv() throws Exception {
//        UserAccount user = new UserAccount();
//        user.setUserId(userId);
//        UserAccount persistedUser = userService.create(user);
//        Account monetaryAccount = new Account();
//        monetaryAccount.setUser(user);
//        monetaryAccount.setCurrency("EUR");
//        monetaryAccount.setDescription("Joey's wallet");
//        Account persistedMonetaryAccount = monetaryAccountService.create(monetaryAccount);
//        String url = String.format("/api/users/%s/accounts/%d/import/generic",
//                persistedUser.getUserId(), persistedMonetaryAccount.getMonetaryAccountId());
//        mvc.perform(multipart(url)
//                .file("file", GenericCsvImporterControllerIntegrationTest.data.getBytes(StandardCharsets.UTF_8))
//                .with(csrf())
//        ).andExpect(status().isOk());
//    }
//
//}
