//package henkui.api.importer.french_polynesia;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import henkui.domain.user_account.UserAccount;
//import henkui.domain.user_account.UserAccountService;
//import henkui.repository.monetary_account.mapper.MonetaryAccountCache;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.persistence.EntityManager;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//public class BanqueSocredoCsvImporterControllerIntegrationTest {
//
//    private final String userId = "123";
//    @Autowired
//    EntityManager entityManager;
//    @Autowired
//    UserAccountService userService;
//    @Autowired
//    AccountService monetaryAccountService;
//    @Autowired
//    TransactionService monetaryTransactionService;
//    @Autowired
//    BanqueSocredoCsvImporterController importer;
//    @Autowired
//    MonetaryAccountCache monetaryAccountCache;
//
//    // courtesy of Thomas STEGEN
//    public static final String data = """
//            Date comptable; Libellé opération; Débit; Crédit; Solde\r
//            27/07/2020;FACT LS PROXI AFARER  DU 24/07 C.SOC 010084938               ;-2063,000;;65295,000\r
//            24/07/2020;RET DAB CSOC010084938 DU 23/07 AUTOR729874  GAB SOC SHELL P  ;-5000,000;;67358,000\r
//            24/07/2020;FACT NETFLIX.COM      DU 22/07 CB100098641            9,99USD;-1061,000;;72358,000\r
//            23/07/2020;RET DAB CSOC010084938 DU 22/07 AUTOR674982  GAB SOC PAEA 2   ;-2000,000;;33419,000\r
//            23/07/2020;RET DAB CSOC010084938 DU 22/07 AUTOR676887  GAB SOC TIPAERU  ;-4000,000;;35419,000\r
//            23/07/2020;RET DAB CSOC010084938 DU 22/07 AUTOR676853  GAB SOC TIPAERU  ;-20000,000;;39419,000\r
//            23/07/2020;FACT TAMANU PAPARA    DU 22/07 C.SOC 010084938               ;-3094,000;;59419,000\r
//            23/07/2020;FACT YOWKO PIZZA TAM  DU 21/07 C.SOC 010084938               ;-3400,000;;62513,000\r
//            23/07/2020;FACT HYPER DEPOT      DU 21/07 C.SOC 010084938               ;-36260,000;;65913,000
//            """;
//
//    @Test
//    @WithMockUser(username = userId)
//    void importData() throws IOException {
//        UserAccount user = new UserAccount();
//        user.setUserId(userId);
//        UserAccount persistedUser = userService.create(user);
//        Account monetaryAccount = new Account();
//        monetaryAccount.setUser(user);
//        monetaryAccount.setCurrency("XPF");
//        monetaryAccount.setDescription("Banque Socredo");
//        Account persistedMonetaryAccount = monetaryAccountService.create(monetaryAccount);
//        MultipartFile multipartFile = new MockMultipartFile("data.csv", data.getBytes(StandardCharsets.UTF_8));
//        importer.importData(multipartFile, persistedUser.getUserId(), persistedMonetaryAccount.getMonetaryAccountId());
//        monetaryAccountCache.flush();
//        entityManager.flush();
//        entityManager.clear();
//        Optional<Account> optionalMonetaryAccount = monetaryAccountService
//                .find(userId, persistedMonetaryAccount.getMonetaryAccountId());
//        assertTrue(optionalMonetaryAccount.isPresent());
//
//        assertEquals(9, optionalMonetaryAccount.get().getTransactions().size());
//
//        Transaction transaction = optionalMonetaryAccount.get().getTransactions().get(0);
//        assertEquals(LocalDate.of(2020, 7, 27), transaction.getAccountingDate());
//        assertEquals(-2063, transaction.getAmount());
//        assertEquals("FACT LS PROXI AFARER  DU 24/07 C.SOC 010084938", transaction.getDescription());
//    }
//
//}
