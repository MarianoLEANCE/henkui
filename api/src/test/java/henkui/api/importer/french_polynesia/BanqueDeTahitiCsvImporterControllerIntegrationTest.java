//package henkui.api.importer.french_polynesia;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import henkui.domain.user_account.UserAccount;
//import henkui.domain.user_account.UserAccountService;
//import henkui.repository.monetary_account.mapper.MonetaryAccountCache;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.persistence.EntityManager;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//public class BanqueDeTahitiCsvImporterControllerIntegrationTest {
//
//    private final String userId = "123";
//    @Autowired
//    EntityManager entityManager;
//    @Autowired
//    UserAccountService userService;
//    @Autowired
//    AccountService monetaryAccountService;
//    @Autowired
//    TransactionService monetaryTransactionService;
//    @Autowired
//    BanqueDeTahitiCsvImporterController importer;
//    @Autowired
//    MonetaryAccountCache monetaryAccountCache;
//
//    // courtesy of Jonathan Lair
//    public static final String data = """
//            Numéro de compte;Intitulé de l'opération;Code devise;Devise;Montant;Date comptable;N° opération;Date de valeur;Numéro de chèque;Libellé d'opération;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-2500,00;23/06/2020;20200623.000001;23/06/2020;;RET.TIA92496- EASYM-GAB1 23/06                                                                ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-830,00;19/06/2020;20200619.000001;18/06/2020;;PMT TIA.PHARMACIE  18/06/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-2000,00;16/06/2020;20200616.000001;16/06/2020;;RET.TIA92496- EASYM-GAB1 16/06                                                                ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-2150,00;12/06/2020;20200612.000001;11/06/2020;;PMT TIA.ARAKA GREN 11/06/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-3385,00;08/06/2020;20200608.000002;05/06/2020;;PMT TIA.SARL PATIS 05/06/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";2520,00;04/06/2020;20200604.000001;04/06/2020;;VIR DE 2230/SCPS                4929616 03/06/20 AM                                           ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-12000,00;04/06/2020;20200604.000002;04/06/2020;;VIRT ACTIVITE ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-1200,00;03/06/2020;20200603.000001;02/06/2020;;PMT TIA.HEAVEN COF 02/06/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-350,00;02/06/2020;20200602.000001;02/06/2020;;ABON TIARE NET 2                REF003095918/TVA           40, ABONNEMENT                     ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-2260,00;02/06/2020;20200602.000002;29/05/2020;;PMT TIA.SARL MONA  29/05/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-12150,00;02/06/2020;20200602.000003;02/06/2020;;PMT TIA.RESTAURANT 30/05/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";-3150,00;02/06/2020;20200602.000004;29/05/2020;;PMT TIA.L EMPORTER 29/05/20                                                                   ;\r
//            "FR7111111111111111111111111";"LAIR JONATHAN";"XPF";"FR CFP";31615,00;02/06/2020;20200602.000005;29/05/2020;;VIRT FR JLAIR                                              ;""";
//
//    @Test
//    @WithMockUser(username = userId)
//    void importData() throws IOException {
//        UserAccount user = new UserAccount();
//        user.setUserId(userId);
//        UserAccount persistedUser = userService.create(user);
//        Account monetaryAccount = new Account();
//        monetaryAccount.setUser(user);
//        monetaryAccount.setCurrency("XPF");
//        monetaryAccount.setDescription("Banque de Polynésie");
//        Account persistedMonetaryAccount = monetaryAccountService.create(monetaryAccount);
//        MultipartFile multipartFile = new MockMultipartFile("data.csv", data.getBytes(StandardCharsets.UTF_8));
//        importer.importData(multipartFile, persistedUser.getUserId(), persistedMonetaryAccount.getMonetaryAccountId());
//        monetaryAccountCache.flush();
//        entityManager.flush();
//        entityManager.clear();
//        Optional<Account> optionalMonetaryAccount = monetaryAccountService
//                .find(userId, persistedMonetaryAccount.getMonetaryAccountId());
//        assertTrue(optionalMonetaryAccount.isPresent());
//
//        assertEquals(13, optionalMonetaryAccount.get().getTransactions().size());
//
//        Transaction transaction = optionalMonetaryAccount.get().getTransactions().get(0);
//        assertEquals(LocalDate.of(2020, 6, 23), transaction.getAccountingDate());
//        assertEquals(-2500, transaction.getAmount());
//        assertEquals("RET.TIA92496- EASYM-GAB1 23/06", transaction.getDescription());
//    }
//
//}
