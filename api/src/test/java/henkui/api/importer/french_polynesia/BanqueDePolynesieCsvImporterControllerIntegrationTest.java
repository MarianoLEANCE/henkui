//package henkui.api.importer.french_polynesia;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import henkui.domain.user_account.UserAccount;
//import henkui.domain.user_account.UserAccountService;
//import henkui.repository.monetary_account.mapper.MonetaryAccountCache;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.persistence.EntityManager;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//class BanqueDePolynesieCsvImporterControllerIntegrationTest {
//
//    private final String userId = "123";
//    @Autowired
//    EntityManager entityManager;
//    @Autowired
//    UserAccountService userService;
//    @Autowired
//    AccountService monetaryAccountService;
//    @Autowired
//    TransactionService monetaryTransactionService;
//    @Autowired
//    BanqueDePolynesieCsvImporterController importer;
//    @Autowired
//    MonetaryAccountCache monetaryAccountCache;
//
//    public static final String data = """
//            Date comptable;Libelle;Date valeur;Montant;Code operation
//            30/12/2021;FRAIS GESTION PRELEVEMENT 2021;2021-12-30;-1200;DIV
//            29/12/2021;ECB2612PDG ST SHELL 36,22EUR;2021-12-29;-4322;ECB
//            27/12/2021;ECB2412MR BRICOLAGE 9,23EUR;2021-12-27;-1101;ECB
//            07/12/2021;ECB0512MARINA EXPRE 51,55EUR;2021-12-07;-6152;ECB
//            """;
//
//    @Test
//    @WithMockUser(username = userId)
//    void importData() throws IOException {
//        UserAccount user = new UserAccount();
//        user.setUserId(userId);
//        UserAccount persistedUser = userService.create(user);
//        Account monetaryAccount = new Account();
//        monetaryAccount.setUser(user);
//        monetaryAccount.setCurrency("XPF");
//        monetaryAccount.setDescription("Banque de Polynésie");
//        Account persistedMonetaryAccount = monetaryAccountService.create(monetaryAccount);
//        MultipartFile multipartFile = new MockMultipartFile("data.csv", data.getBytes(StandardCharsets.UTF_8));
//        importer.importData(multipartFile, persistedUser.getUserId(), persistedMonetaryAccount.getMonetaryAccountId());
//        monetaryAccountCache.flush();
//        entityManager.flush();
//        entityManager.clear();
//        Optional<Account> optionalMonetaryAccount = monetaryAccountService
//                .find(userId, persistedMonetaryAccount.getMonetaryAccountId());
//        assertTrue(optionalMonetaryAccount.isPresent());
//
//        assertEquals(4, optionalMonetaryAccount.get().getTransactions().size());
//
//        Transaction transaction = optionalMonetaryAccount.get().getTransactions().get(0);
//        assertEquals(LocalDate.of(2021, 12, 30), transaction.getAccountingDate());
//        assertEquals(-1200, transaction.getAmount());
//        assertEquals("FRAIS GESTION PRELEVEMENT 2021", transaction.getDescription());
//    }
//
//    @Test
//    @WithMockUser(username = userId)
//    void importData_noAccount() throws IOException {
//        MultipartFile multipartFile = new MockMultipartFile("data.csv", data.getBytes(StandardCharsets.UTF_8));
//        var response = importer.importData(multipartFile, userId, 1);
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//    }
//
//}
