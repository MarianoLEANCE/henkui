//package henkui.api.importer.french_polynesia;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.Transaction;
//import henkui.monetary_transaction.service.TransactionService;
//import henkui.domain.user_account.UserAccount;
//import henkui.domain.user_account.UserAccountService;
//import henkui.repository.monetary_account.mapper.MonetaryAccountCache;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mock.web.MockMultipartFile;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.persistence.EntityManager;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.time.LocalDate;
//import java.util.Optional;
//
//import static henkui.api.importer.france.AmericanExpressEurCsvImporterControllerIntegrationTest.data;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//public class AmericanExpressCsvImporterControllerIntegrationTest {
//
//    private final String userId = "123";
//    @Autowired
//    EntityManager entityManager;
//    @Autowired
//    UserAccountService userService;
//    @Autowired
//    AccountService monetaryAccountService;
//    @Autowired
//    TransactionService monetaryTransactionService;
//    @Autowired
//    AmericanExpressCsvImporterController importer;
//    @Autowired
//    MonetaryAccountCache monetaryAccountCache;
//
//    @Test
//    @WithMockUser(username = userId)
//    void importData() throws IOException {
//        UserAccount user = new UserAccount();
//        user.setUserId(userId);
//        UserAccount persistedUser = userService.create(user);
//        Account monetaryAccount = new Account();
//        monetaryAccount.setUser(user);
//        monetaryAccount.setCurrency("XPF");
//        monetaryAccount.setDescription("American Express");
//        Account persistedMonetaryAccount = monetaryAccountService.create(monetaryAccount);
//        MultipartFile multipartFile = new MockMultipartFile("data.csv", data.getBytes(StandardCharsets.UTF_8));
//        importer.importData(multipartFile, persistedUser.getUserId(), persistedMonetaryAccount.getMonetaryAccountId());
//        monetaryAccountCache.flush();
//        entityManager.flush();
//        entityManager.clear();
//        Optional<Account> optionalMonetaryAccount = monetaryAccountService
//                .find(userId, persistedMonetaryAccount.getMonetaryAccountId());
//        assertTrue(optionalMonetaryAccount.isPresent());
//
//        assertEquals(13, optionalMonetaryAccount.get().getTransactions().size());
//
//        Transaction transaction = optionalMonetaryAccount.get().getTransactions().get(0);
//        assertEquals(LocalDate.of(2021, 10, 22), transaction.getAccountingDate());
//        assertEquals(-357, transaction.getAmount());
//        assertEquals("APPLE.COM/BILL          HOLLYHILL", transaction.getDescription());
//    }
//
//}
