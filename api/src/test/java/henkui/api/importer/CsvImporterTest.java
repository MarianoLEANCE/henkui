//package henkui.api.importer;
//
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertArrayEquals;
//
//class CsvImporterTest {
//
//    CsvParser csvImporter = new CsvParser();
//
//    @Test
//    void records_unquoted() {
//        String[] actual = csvImporter.split("2020-01-01,123456,banana", ",");
//        assertArrayEquals(new String[]{"2020-01-01", "123456", "banana"}, actual);
//    }
//
//    @Test
//    void records_quoted() {
//        String[] actual = csvImporter.split("2020-01-01,\"red,green,blue\",444", ",");
//        assertArrayEquals(new String[]{"2020-01-01", "red,green,blue", "444"}, actual);
//    }
//
//}
