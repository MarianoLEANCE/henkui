//package henkui.api.monetary_transaction;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.service.AccountService;
//import henkui.monetary_transaction.controller.TransactionController;
//import henkui.monetary_transaction.service.TransactionService;
//import henkui.repository.monetary_account.entity.MonetaryAccountEntity;
//import henkui.repository.monetary_account.repository.MonetaryAccountEntityRepository;
//import henkui.monetary_account.repository.AccountRepository;
//import henkui.repository.monetary_transaction.entity.MonetaryTransactionEntity;
//import henkui.repository.monetary_transaction.repository.MonetaryTransactionEntityRepository;
//import henkui.repository.user_account.entity.UserAccountEntity;
//import henkui.repository.user_account.repository.UserAccountEntityRepository;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.test.context.support.WithMockUser;
//
//import javax.persistence.EntityManager;
//import java.time.LocalDate;
//import java.time.Month;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//class MonetaryTransactionControllerIntegrationTest {
//
//    @Autowired
//    EntityManager entityManager;
//
//    @Autowired
//    UserAccountEntityRepository userAccountEntityRepository;
//    @Autowired
//    MonetaryAccountEntityRepository monetaryAccountEntityRepository;
//    @Autowired
//    MonetaryTransactionEntityRepository monetaryTransactionEntityRepository;
//
//    @Autowired
//    AccountService monetaryAccountService;
//    @Autowired
//    TransactionService monetaryTransactionService;
//    @Autowired
//    TransactionController monetaryTransactionController;
//
//    @Autowired
//    AccountRepository monetaryAccountRepository;
//
//    private final String userId = "123";
//
//    @Test
//    @WithMockUser(username = userId)
//    public void split() {
//        UserAccountEntity userAccountEntity = new UserAccountEntity();
//        userAccountEntity.setUserId(userId);
//        userAccountEntity = userAccountEntityRepository.save(userAccountEntity);
//
//        MonetaryAccountEntity monetaryAccountEntity = new MonetaryAccountEntity();
//        monetaryAccountEntity.setCurrency("USD");
//        monetaryAccountEntity.setDescription("checking account");
//        monetaryAccountEntity.setUser(userAccountEntity);
//        monetaryAccountEntity = monetaryAccountEntityRepository.save(monetaryAccountEntity);
//
//        MonetaryTransactionEntity monetaryTransactionEntity = new MonetaryTransactionEntity();
//        monetaryTransactionEntity.setAccountingDate(LocalDate.of(2021, Month.JANUARY, 10));
//        monetaryTransactionEntity.setBudgetYear(2021);
//        monetaryTransactionEntity.setBudgetMonth(1);
//        monetaryTransactionEntity.setAmount(300);
//        monetaryTransactionEntity.setDescription("");
//        monetaryTransactionEntity.setCategory("");
//        monetaryTransactionEntity.setSubcategory("");
//        monetaryTransactionEntity.setMonetaryAccount(monetaryAccountEntity);
//        monetaryTransactionEntity = monetaryTransactionEntityRepository.save(monetaryTransactionEntity);
//
//        monetaryTransactionController.split(userId, monetaryAccountEntity.getId(), monetaryTransactionEntity.getId(),
//                new TransactionController.SplitPayload(-100d));
//
//        entityManager.flush();
//        entityManager.clear();
//
//        Optional<MonetaryAccountEntity> afterSplit = monetaryAccountEntityRepository
//                .findById(monetaryAccountEntity.getId());
//        assertTrue(afterSplit.isPresent());
//        assertEquals(2, afterSplit.get().getTransactions().size());
//    }
//
//}
