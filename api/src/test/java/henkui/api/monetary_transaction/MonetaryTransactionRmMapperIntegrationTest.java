//package henkui.api.monetary_transaction;
//
//import henkui.HenkuiIntegrationTest;
//import henkui.monetary_account.Account;
//import henkui.monetary_transaction.Transaction;
//import henkui.domain.user_account.UserAccount;
//import henkui.monetary_transaction.controller.rm.TransactionRm;
//import henkui.monetary_transaction.controller.mapper.TransactionRmMapper;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.hateoas.IanaLinkRelations;
//import org.springframework.hateoas.Link;
//
//import java.time.LocalDate;
//import java.util.Optional;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//@HenkuiIntegrationTest
//class MonetaryTransactionRmMapperIntegrationTest {
//
//    @Autowired
//    TransactionRmMapper mapper;
//
//    @Test
//    void map() {
//
//        String userId = "bob";
//        int accountId = 1;
//        int transactionId = 1;
//        int amount = -2;
//        String description = "bread";
//        LocalDate accountingDate = LocalDate.of(2020, 8, 8);
//        int budgetYear = 2020;
//        int budgetMonth = 8;
//
//        UserAccount userAccount = new UserAccount();
//        userAccount.setUserId(userId);
//
//        Account monetaryAccount = new Account();
//        monetaryAccount.setMonetaryAccountId(accountId);
//        monetaryAccount.setUser(userAccount);
//
//        Transaction monetaryTransaction = new Transaction();
//        monetaryTransaction.setMonetaryAccount(monetaryAccount);
//        monetaryTransaction.setMonetaryTransactionId(transactionId);
//        monetaryTransaction.setAmount(amount);
//        monetaryTransaction.setDescription(description);
//        monetaryTransaction.setAccountingDate(accountingDate);
//        monetaryTransaction.setBudgetYear(budgetYear);
//        monetaryTransaction.setBudgetMonth(budgetMonth);
//
//        TransactionRm actual = mapper.map(monetaryTransaction);
//
//        Optional<Link> self = actual.getLink(IanaLinkRelations.SELF);
//        assertTrue(self.isPresent());
//        assertTrue(self.get().getHref()
//                .endsWith(String.format("api/users/%s/accounts/%d/transactions/%d", userId, accountId, transactionId)));
//        assertEquals(amount, actual.getAmount());
//        assertEquals(description, actual.getDescription());
//        assertEquals(accountingDate, actual.getAccountingDate());
//        assertEquals(budgetYear, actual.getBudgetYear());
//        assertEquals(budgetMonth, actual.getBudgetMonth());
//    }
//
//}
