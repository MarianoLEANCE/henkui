package henkui.budget.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import henkui.HenkuiIntegrationTest;
import henkui.IntegrationTestUtils;
import henkui.budget.Budget;
import henkui.budget.controller.rm.BudgetRm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class BudgetControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    IntegrationTestUtils utils;

    @Test
    @WithMockUser
    void create() throws Exception {
        BudgetRm rm = utils.rm(utils.budget());
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    void create_withId() throws Exception {
        BudgetRm rm = utils.rm(utils.budget());
        rm.setId(1); // id should be null
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_invalid() throws Exception {
        BudgetRm rm = new BudgetRm(); // no budget year
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void find() throws Exception {
        Budget budget = utils.create(utils.budget());
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/" + budget.getId()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void find_notFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void list() throws Exception {
        Budget budget = utils.create(utils.budget());
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.budgets.size()", equalTo(1)))
                .andExpect(jsonPath("$._embedded.budgets[0].budgetYear", equalTo(budget.getYear())));
    }

    @Test
    @WithMockUser
    void update() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetRm rm = utils.rm(budget);
        rm.setBudgetYear(2023);
        mvc.perform(MockMvcRequestBuilders.put("/api/budgets/%d".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.budgetYear", equalTo(rm.getBudgetYear())));
    }

    @Test
    @WithMockUser
    void update_BadId() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetRm rm = utils.rm(budget);
        rm.setId(-1);
        mvc.perform(MockMvcRequestBuilders.put("/api/budgets/%d".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void delete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/api/budgets/0"))
                .andExpect(status().isOk());
    }

}
