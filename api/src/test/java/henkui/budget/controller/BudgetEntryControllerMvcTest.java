package henkui.budget.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import henkui.HenkuiIntegrationTest;
import henkui.IntegrationTestUtils;
import henkui.account.Account;
import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.budget.controller.rm.BudgetEntryRm;
import henkui.transaction.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.persistence.EntityManager;

import static org.hamcrest.Matchers.comparesEqualTo;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@HenkuiIntegrationTest
class BudgetEntryControllerMvcTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    EntityManager entityManager;
    @Autowired
    IntegrationTestUtils utils;

    @Test
    @WithMockUser
    void create() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntryRm rm = utils.budgetEntryRm();
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries"
                                .formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser
    void create_withId() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntryRm rm = utils.budgetEntryRm();
        rm.setId(1);
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noAmount() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntryRm rm = utils.budgetEntryRm();
        rm.setAmount(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noCategory() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntryRm rm = utils.budgetEntryRm();
        rm.setCategory(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_noSubcategory() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntryRm rm = utils.budgetEntryRm();
        rm.setSubcategory(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void create_similarExists() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        BudgetEntryRm rm = utils.rm(budgetEntry);
        rm.setId(null);
        mvc.perform(MockMvcRequestBuilders.post("/api/budgets/%d/entries".formatted(budget.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isConflict());
    }

    @Test
    @WithMockUser
    void find() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/%d/entries/%d"
                        .formatted(budget.getId(), budgetEntry.getId())))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void find_notFound() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/0/entries/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void list() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/%d/entries".formatted(budget.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.budgetEntries.size()", equalTo(1)))
                .andExpect(jsonPath("$._embedded.budgetEntries[0].id",
                        equalTo(budgetEntry.getId())))
                .andExpect(jsonPath("$._embedded.budgetEntries[0].category",
                        equalTo(budgetEntry.getCategory())))
                .andExpect(jsonPath("$._embedded.budgetEntries[0].subcategory",
                        equalTo(budgetEntry.getSubcategory())));
    }

    @Test
    @WithMockUser
    void spendingRate() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        Account account = utils.create(utils.account());
        Transaction transaction = utils.create(utils.transaction(account));
        utils.create(utils.allocation(transaction, budgetEntry));
        entityManager.flush();
        entityManager.clear();
        mvc.perform(MockMvcRequestBuilders.get("/api/budgets/%d/entries/%d/spending-rate"
                        .formatted(budget.getId(), budgetEntry.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cumulativeExpenses[0].actualAmount", comparesEqualTo(0d)))
                .andExpect(jsonPath("$.cumulativeExpenses[229].actualAmount", comparesEqualTo(-3d)))
                .andExpect(jsonPath("$.cumulativeExpenses[364].maxPerYear", comparesEqualTo(-1000d)));
    }

    @Test
    @WithMockUser
    void update() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        BudgetEntryRm rm = utils.rm(budgetEntry);
        rm.setAmount(2000d);
        mvc.perform(MockMvcRequestBuilders.put("/api/budgets/%d/entries/%d"
                                .formatted(budget.getId(), budgetEntry.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount", comparesEqualTo(2000d)));
    }

    @Test
    @WithMockUser
    void update_BadId() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        BudgetEntryRm rm = utils.rm(budgetEntry);
        rm.setId(-1);
        mvc.perform(MockMvcRequestBuilders.put("/api/budgets/%d/entries/%d"
                                .formatted(budget.getId(), budgetEntry.getId()))
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(rm)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser
    void delete() throws Exception {
        Budget budget = utils.create(utils.budget());
        BudgetEntry budgetEntry = utils.create(utils.budgetEntry(budget));
        mvc.perform(MockMvcRequestBuilders.delete("/api/budgets/%d/entries/%d"
                        .formatted(budget.getId(), budgetEntry.getId())))
                .andExpect(status().isOk());
    }

}
