package henkui;

import henkui.account.Account;
import henkui.account.repository.AccountRepository;
import henkui.budget.Budget;
import henkui.budget.BudgetEntry;
import henkui.budget.controller.mapper.BudgetEntryRmMapper;
import henkui.budget.controller.mapper.BudgetRmMapper;
import henkui.budget.controller.rm.BudgetEntryRm;
import henkui.budget.controller.rm.BudgetRm;
import henkui.budget.repository.BudgetEntryRepository;
import henkui.budget.repository.BudgetRepository;
import henkui.transaction.Allocation;
import henkui.transaction.Transaction;
import henkui.transaction.controller.mapper.TransactionRmMapper;
import henkui.transaction.controller.rm.TransactionRm;
import henkui.transaction.repository.AllocationRepository;
import henkui.transaction.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class IntegrationTestUtils {

    private final AccountRepository accountRepository;
    private final AllocationRepository allocationRepository;
    private final BudgetRepository budgetRepository;
    private final BudgetEntryRepository budgetEntryRepository;
    private final TransactionRepository transactionRepository;
    private final BudgetRmMapper budgetRmMapper;
    private final BudgetEntryRmMapper budgetEntryRmMapper;
    private final TransactionRmMapper transactionRmMapper;

    public Budget budget() {
        Budget budget = new Budget();
        budget.setYear(2022);
        return budget;
    }

    public BudgetEntry budgetEntry(Budget budget) {
        BudgetEntry entry = new BudgetEntry();
        entry.setCategory("groceries");
        entry.setSubcategory("food");
        entry.setAmount(-1000d);
        entry.setBudget(budget);
        entry.setAllocations(new ArrayList<>());
        return entry;
    }

    public BudgetEntryRm budgetEntryRm() {
        BudgetEntryRm rm = new BudgetEntryRm();
        rm.setCategory("groceries");
        rm.setSubcategory("food");
        rm.setAmount(-1000d);
        return rm;
    }

    public Account account() {
        Account account = new Account();
        account.setDescription("checking account");
        return account;
    }

    public Transaction transaction(Account account) {
        Transaction transaction = new Transaction();
        transaction.setAccountingDate(LocalDate.of(2022, Month.AUGUST, 15));
        transaction.setBudgetYear(transaction.getAccountingDate().getYear());
        transaction.setBudgetMonth(transaction.getAccountingDate().getMonthValue());
        transaction.setAmount(-3d);
        transaction.setDescription("spaghetti");
        transaction.setAccount(account);
        transaction.setAllocations(new ArrayList<>());
        return transaction;
    }

    public Allocation allocation(Transaction transaction, BudgetEntry budgetEntry) {
        Allocation allocation = new Allocation();
        allocation.setAmount(-3d);
        allocation.setTransaction(transaction);
        allocation.setBudgetEntry(budgetEntry);
        return allocation;
    }

    public BudgetRm rm(Budget budget) {
        return budgetRmMapper.map(budget);
    }

    public BudgetEntryRm rm(BudgetEntry budgetEntry) {
        return budgetEntryRmMapper.map(budgetEntry);
    }

    public TransactionRm rm(Transaction transaction) {
        return transactionRmMapper.map(transaction);
    }

    public Budget create(Budget budget) {
        return budgetRepository.save(budget);
    }

    public BudgetEntry create(BudgetEntry budgetEntry) {
        return budgetEntryRepository.save(budgetEntry);
    }

    public Account create(Account account) {
        return accountRepository.save(account);
    }

    public Transaction create(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    public Allocation create(Allocation allocation) {
        return allocationRepository.save(allocation);
    }

}
