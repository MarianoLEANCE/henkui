create table budget
(
    budget_id   integer primary key not null auto_increment,
    budget_year integer             not null
);

create table budget_entry
(
    budget_entry_id int          not null primary key auto_increment,
    budget_id       int          not null,
    amount          double       not null,
    category        varchar(100) not null,
    subcategory     varchar(100) not null,
    constraint budget_entry_budget_fk foreign key (budget_id) references budget (budget_id)
);

create table monetary_account
(
    monetary_account_id int          not null primary key auto_increment,
    description         varchar(500) not null
);

create table monetary_transaction
(
    monetary_transaction_id int          not null primary key auto_increment,
    accounting_date         date         not null,
    budget_year             int          not null,
    budget_month            int          not null,
    amount                  double       not null,
    monetary_account_id     int          not null,
    description             varchar(500) not null,
    constraint transaction_account_fk foreign key (monetary_account_id)
        references monetary_account (monetary_account_id)
);

create table allocation
(
    allocation_id           int    not null primary key auto_increment,
    monetary_transaction_id int    not null,
    budget_entry_id         int    not null,
    amount                  double not null,
    constraint allocation_transaction_fk foreign key (monetary_transaction_id)
        references monetary_transaction (monetary_transaction_id),
    constraint allocation_budget_entry_fk foreign key (budget_entry_id)
        references budget_entry (budget_entry_id)
);
