# [Henkui](https://www.henkui.net)

[![pipeline status](https://gitlab.com/artificersoftware/henkui/badges/master/pipeline.svg)](https://gitlab.com/artificersoftware/Henkui/-/commits/master)
[![coverage report](https://gitlab.com/artificersoftware/henkui/badges/master/coverage.svg)](https://gitlab.com/artificersoftware/Henkui/-/commits/master)

Henkui is a free and open-source personal accounting web application. Henkui's goal is help people do their
accounting in the least amount of time and visualize their budget and assets.

## Inputs

Henkui tries to simplify as much as possible data inputs. Some banks may have export capabilities to various file
formats like csv. Henkui will provide multiple import tools to process such data. If your file format is not please get 
in touch.

## Visualize your expenses

Making a budget is nearly impossible without knowing where money is spent. Henkui will help you visualize your expenses
and classify them.

## Set your goals

Are you still on track? Set up goals and visualize them easily.

## Contribute

Become a patron for Henkui on [my Patreon page](https://www.patreon.com/artificer_software).

## Moving forward

Hosting Henkui online was an incredible experience but without proper monetization it could not last forever.
The project still lives but will become an on-premise only product.

The current goals are :
- Use JDBC authentication for primary authentication.
- Desktop only experience to lower complexity.
- Manage database with liquibase, flyway or equivalent
- Use spring native for minimal footprint
- Simplifying the code
- Simplifying the UI
