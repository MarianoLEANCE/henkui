FROM node:18 AS build-angular
WORKDIR /usr/src/app
RUN npm install -g @angular/cli
COPY gui/package.json gui/package-lock.json ./
RUN npm install
COPY gui .
RUN ng build --configuration production

FROM maven:3-eclipse-temurin-19 AS build-java
WORKDIR /usr/src/app
COPY api/pom.xml .
COPY api/src/main/java/henkui/Henkui.java src/main/java/henkui/Henkui.java
RUN mvn package -D maven.test.skip=true
COPY api .
COPY --from=build-angular /usr/src/app/dist/gui src/main/resources/static
RUN mvn package -D maven.test.skip=true

FROM eclipse-temurin:19
CMD java -jar app.jar
COPY --from=build-java /usr/src/app/target/*.jar app.jar
