export const env = {
  production: true,
  apiUrl: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
  currency: {code: 'XPF', fractionDigits: 0}
};
