import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {AccountScreenComponent} from "./account/account-screen/account-screen.component";
import {TransactionScreenComponent} from "./transaction/transaction-screen/transaction-screen.component";
import {BudgetScreenComponent} from "./budget/budget-screen/budget-screen.component";
import {AnalyzeScreenComponent} from "./transaction/analyze-screen/analyze-screen.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'accounts', component: AccountScreenComponent},
  {path: 'budgets', component: BudgetScreenComponent},
  {path: 'analyze', component: AnalyzeScreenComponent},
  {path: 'transactions', component: TransactionScreenComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
