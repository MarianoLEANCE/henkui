import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AccountEditDialogComponent} from './account/account-edit-dialog/account-edit-dialog.component'
import {AccountImportDialogComponent} from "./account/import-edit-dialog/account-import-dialog.component";
import {AccountScreenComponent} from './account/account-screen/account-screen.component';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BudgetEditDialogComponent} from "./budget/budget-edit-dialog/budget-edit-dialog.component";
import {BudgetScreenComponent} from './budget/budget-screen/budget-screen.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {MainNavComponent} from './main-nav/main-nav.component';
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatDialogModule} from "@angular/material/dialog";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatListModule} from "@angular/material/list";
import {MatNativeDateModule} from "@angular/material/core";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatSelectModule} from "@angular/material/select";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MonthNamePipe} from "./pipes/month-name/month-name.pipe";
import {TransactionEditDialogComponent} from "./transaction/transaction-edit-dialog/transaction-edit-dialog.component";
import {TransactionScreenComponent} from "./transaction/transaction-screen/transaction-screen.component";
import {
  TransactionAllocateDialogComponent
} from "./transaction/transaction-allocate-dialog/transaction-allocate-dialog.component";
import {AnalyzeScreenComponent} from "./transaction/analyze-screen/analyze-screen.component";
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {
  BudgetEntrySpendingRateDialogComponent
} from "./budget/budget-entry-spending-rate-dialog/budget-entry-spending-rate-dialog.component";

@NgModule({
  declarations: [
    AccountEditDialogComponent,
    AccountImportDialogComponent,
    AccountScreenComponent,
    AppComponent,
    BudgetEditDialogComponent,
    BudgetEntrySpendingRateDialogComponent,
    BudgetScreenComponent,
    HomeComponent,
    MainNavComponent,
    MonthNamePipe,
    TransactionAllocateDialogComponent,
    TransactionEditDialogComponent,
    TransactionScreenComponent,
    AnalyzeScreenComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    NgxChartsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
