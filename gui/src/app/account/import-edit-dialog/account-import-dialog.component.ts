import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AccountService} from "../service/account.service";
import {Account} from "../account";
import {HttpClient} from "@angular/common/http";
import {FormBuilder} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-edit-account-dialog',
  templateUrl: './account-import-dialog.component.html',
  styleUrls: ['./account-import-dialog.component.scss']
})
export class AccountImportDialogComponent implements OnInit {

  service: AccountService;
  selectedImporter?: [string, string];
  // TODO refactor using rest links from the account
  importers = [
    ["American Express", "american-express"],
    ["Banque de Polynésie", "banque-de-polynesie"],
  ];
  selectedFileName?: string;
  formData?: FormData;

  constructor(public dialogRef: MatDialogRef<AccountImportDialogComponent>,
              private http: HttpClient,
              private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public account: Account,
              service: AccountService) {
    this.service = service;
  }

  ngOnInit(): void { }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onFileSelected(event: any) {
    if (event.target.files.length > 0) {
      this.selectedFileName = event.target.files[0].name;
      this.formData = new FormData();
      this.formData.append('file', event.target.files[0]);
      this.formData.append('reportProgress', "true");
    }
  }

  onImportClick() {
    const url = this.account._links!.self.href + '/import/' + this.selectedImporter![1];
    const httpOptions = {
      // headers: new HttpHeaders({"Content-Type": "multipart/form-data"}),
      withCredentials: true
    };
    this.http.post<any>(url, this.formData, httpOptions)
      .subscribe(() => {
          this.snackBar.open(`Transactions imported.`, 'Dismiss', {duration: 1000});
          this.dialogRef.close();
        },
        err => this.snackBar.open(err.data, `Dismiss`)
      );
    this.dialogRef.close('imported');
  }

}
