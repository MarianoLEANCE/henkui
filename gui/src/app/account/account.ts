import {Link, Links} from '../model/link';

export class Account {

  id?: number;
  // transactions: Transaction[];
  description: string;
  _links?: AccountLinks;

  static of(data: Account): Account {
    return new Account(data.description, data.id, data._links);
  }

  constructor(description: string,
              id?: number, _links?: AccountLinks) {
    this.description = description;
    this.id = id;
    this._links = _links;
  }

}

export class AccountLinks extends Links {

  transactions: Link;

  constructor(self: Link, transactions: Link) {
    super(self);
    this.transactions = transactions;
  }

}
