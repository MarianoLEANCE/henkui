import {Links} from "../model/link";
import {Account} from "./account";
import {ResourceCollection} from "../model/resource-collection";

export class AccountsResourceCollection extends ResourceCollection<Accounts> {

  constructor(_embedded: Accounts, _links: Links) {
    super(_embedded, _links);
  }

}

export class Accounts {

  accounts: Account[];

  constructor(accounts: Account[]) {
    this.accounts = accounts;
  }

}
