import {Component, OnInit} from '@angular/core';
import {AccountService} from "../service/account.service";
import {Account} from "../account";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from "@angular/material/dialog";
import {AccountEditDialogComponent} from "../account-edit-dialog/account-edit-dialog.component";
import {AccountImportDialogComponent} from "../import-edit-dialog/account-import-dialog.component";

@Component({
  selector: 'app-account-screen',
  templateUrl: './account-screen.component.html',
  styleUrls: ['./account-screen.component.scss']
})
export class AccountScreenComponent implements OnInit {

  newAccount: Account = new Account('');
  accounts: Account[] = [];
  displayedColumns: string[] = ['description', 'actions'];

  constructor(private dialog: MatDialog,
              private accountService: AccountService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchAccounts();
  }

  fetchAccounts() {
    this.accountService.list().subscribe(data => {
      this.accounts = data._embedded.accounts;
    })
  }

  create() {
    this.accountService.create(this.newAccount)
      .subscribe(() => this.fetchAccounts());
  }

  delete(account: Account) {
    this._snackBar.open(`Confirm delete account ${account.description}.`, `Delete`, {
      duration: 5000
    }).onAction()
      .subscribe(() => this.accountService.delete(account)
        .subscribe(() => this.fetchAccounts()));
  }

  editDialog(account: Account) {
    const dialogRef = this.dialog.open(AccountEditDialogComponent, {width: '408px', data: account});
    dialogRef.afterClosed().subscribe(event => {
      if (event === 'updated') {
        this._snackBar.open(`Account updated.`, 'Dismiss', {duration: 1000});
      }
    });
  }

  importDialog(account: Account) {
    const dialogRef = this.dialog.open(AccountImportDialogComponent, {width: '408px', data: account});
    dialogRef.afterClosed().subscribe(event => {
      if (event === 'updated') {
        this._snackBar.open(`Transactions imported.`, 'Dismiss', {duration: 1000});
      }
    });
  }

}
