import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AccountService} from "../service/account.service";
import {Account} from "../account";

@Component({
  selector: 'app-edit-account-dialog',
  templateUrl: './account-edit-dialog.component.html',
  styleUrls: ['./account-edit-dialog.component.scss']
})
export class AccountEditDialogComponent implements OnInit {

  service: AccountService;
  copy: Account;

  constructor(
    public dialogRef: MatDialogRef<AccountEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Account,
    service: AccountService) {
    this.copy = Account.of(data);
    this.update(this.data, this.copy);
    this.service = service;
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): Account {
    this.update(this.copy, this.data);
    this.service.update(this.data).subscribe(result => console.log(result));
    this.dialogRef.close('updated');
    return this.data;
  }

  ngOnInit(): void { }

  update(from: Account, to: Account): void {
    to.description = from.description.trim();
  }
}
