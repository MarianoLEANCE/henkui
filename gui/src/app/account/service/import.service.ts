import {Injectable} from '@angular/core';
import {env} from '../../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Account} from '../account';
import {AccountsResourceCollection} from "../accounts-resource-collection";

@Injectable({providedIn: 'root'})
export class AccountService {

  url: string = env.apiUrl + '/api/accounts/{account-id}/import/';

  constructor(private http: HttpClient) { }

  public importAmericanExpressTransactions(account: Account): Observable<Account> {
    return this.http.post<Account>(this.url, account, {withCredentials: true});
  }

  update(account: Account): Observable<Account> {
    return this.http.put<Account>(<string>account._links!.self.href, account, {withCredentials: true});
  }

  delete(account: Account): Observable<HttpResponse<any>> {
    return this.http.delete(<string>account._links!.self.href, {observe: "response", withCredentials: true});
  }

}
