import {env} from "../../environments/environment";

export class NumberFormatter {

  /**
   * Formats a number and pads it with unbreakable spaces.
   */
  static format(numberFormat: Intl.NumberFormat, maxLength: number, x: number): string {
    return numberFormat.format(x).padStart(maxLength, '\xa0');
    // return numberFormat.format(x).padStart(maxLength, '_');
  }

  /**
   * Finds the maximum size of a formatted number in an array.
   */
  static maxLength(numberFormat: Intl.NumberFormat, a: number[]): number {
    let min = 0;
    let max = 0;
    a.forEach(x => {
      if (x < min) { min = x; }
      if (x > max) { max = x; }
    })
    return Math.max(numberFormat.format(min).length, numberFormat.format(max).length);
  }

  public static numberFormat(): Intl.NumberFormat {
    return new Intl.NumberFormat(`en-US`, {
      maximumFractionDigits: env.currency.fractionDigits
    });
  }

}
