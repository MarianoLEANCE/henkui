export class LineChartData {

  name: string;
  series: {
    name: string;
    value: number;
  }[];

  constructor(name: string, series: { name: string; value: number }[]) {
    this.name = name;
    this.series = series;
  }

}
