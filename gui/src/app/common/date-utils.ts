export class DateUtils {

  public static parseIsoToCurrentTimeZone(s: string): Date {
    return new Date(Number.parseInt(s.substring(0, 4)),
      Number.parseInt(s.substring(5, 7))-1,
      Number.parseInt(s.substring(8, 10)));
  }

}
