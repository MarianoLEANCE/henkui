export class Allocation {
  category: string;
  subcategory: string;
  amount: number;

  constructor(category: string, subcategory: string, amount: number) {
    this.amount = amount;
    this.category = category;
    this.subcategory = subcategory;
  }
}
