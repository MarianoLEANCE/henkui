export class Links {

  self: Link;

  constructor(self: Link) {
    this.self = self;
  }

}

export class Link {

  href: string;

  constructor(href: string) {
    this.href = href;
  }

}
