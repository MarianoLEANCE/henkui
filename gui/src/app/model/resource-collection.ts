import {Links} from './link';

export class ResourceCollection<T> {

  _embedded: T;
  _links: Links;

  constructor(_embedded: T, _links: Links) {
    this._embedded = _embedded;
    this._links = _links;
  }

}
