import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transaction} from '../transaction';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from "moment";
import {DateUtils} from "../../common/date-utils";
import {TransactionService} from "../transaction-service/transaction.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-edit-transaction-dialog',
  templateUrl: './transaction-edit-dialog.component.html',
  styleUrls: ['./transaction-edit-dialog.component.scss']
})
export class TransactionEditDialogComponent implements OnInit {

  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<TransactionEditDialogComponent>,
              private transactionService: TransactionService,
              private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public actual: Transaction) {
    const accountingDate = DateUtils.parseIsoToCurrentTimeZone(actual.accountingDate);
    this.form = new FormGroup({
      amount: new FormControl(actual.amount, [
        Validators.required, Validators.pattern('^-?[1-9][0-9]*(.[0-9]+)?$')]),
      accountingDate: new FormControl(accountingDate, [Validators.required]),
      budgetMonth: new FormControl(actual.budgetMonth, [Validators.required]),
      budgetYear: new FormControl(actual.budgetYear, [Validators.required]),
      description: new FormControl(actual.description, [Validators.required])
    });
  }

  ngOnInit(): void { }

  accountingDateErrorMessage() {
    if (this.form.controls['accountingDate'].hasError('required')) {
      return `You must select a date.`;
    }
    return '';
  }

  budgetMonthErrorMessage() {
    if (this.form.controls['accountingDate'].hasError('required')) {
      return `You must select a month.`;
    }
    return '';
  }

  budgetYearErrorMessage() {
    if (this.form.controls['accountingDate'].hasError('required')) {
      return `You must select a year.`;
    }
    return '';
  }

  descriptionErrorMessage() {
    if (this.form.controls['description'].hasError('required')) {
      return `You must enter a description.`;
    }
    return '';
  }

  amountErrorMessage() {
    if (this.form.controls['amount'].hasError('required')) {
      return `You must enter a value.`;
    }
    if (this.form.controls['amount'].hasError('pattern')) {
      return `You must enter a non null value.`;
    }
    return '';
  }

  updateBudgetDates() {
    this.form.controls['budgetYear']
      .setValue(this.form.controls['accountingDate'].value.getFullYear());
    this.form.controls['budgetMonth']
      .setValue(this.form.controls['accountingDate'].value.getMonth() + 1);
  }

  onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const transaction = this.actual;
      transaction.amount = this.form.controls['amount'].value;
      transaction.accountingDate = moment(this.form.controls['accountingDate'].value).format("YYYY-MM-DD");
      transaction.budgetMonth = this.form.controls['budgetMonth'].value;
      transaction.budgetYear = this.form.controls['budgetYear'].value;
      transaction.description = this.form.controls['description'].value.trim();
      this.transactionService.update(transaction).subscribe(() => {
        this.snackBar.open(`Transaction updated.`, 'Dismiss', {duration: 1000});
      });
      this.dialogRef.close({action: 'updated', transaction: this.actual});
    }
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

}
