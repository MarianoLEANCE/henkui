export class TransactionAnalysis {

  totalAmount: number;
  totalPerMonth: MonthlyAnalysis[];
  categories: CategoryAnalysis[];

  constructor(totalAmount: number, totalPerMonth: MonthlyAnalysis[], categories: CategoryAnalysis[]) {
    this.totalAmount = totalAmount;
    this.totalPerMonth = totalPerMonth;
    this.categories = categories;
  }

}

export class CategoryAnalysis {

  description: string;
  totalAmount: number;
  totalPerMonth: MonthlyAnalysis[];
  subcategories: SubcategoryAnalysis[];

  constructor(description: string,
              totalAmount: number,
              totalPerMonth: MonthlyAnalysis[],
              subcategories: SubcategoryAnalysis[]) {
    this.description = description;
    this.totalAmount = totalAmount;
    this.totalPerMonth = totalPerMonth;
    this.subcategories = subcategories;
  }

}

export class SubcategoryAnalysis {

  description: string;
  totalAmount: number;
  totalPerMonth: MonthlyAnalysis[];

  constructor(description: string, totalAmount: number, totalPerMonth: MonthlyAnalysis[]) {
    this.description = description;
    this.totalAmount = totalAmount;
    this.totalPerMonth = totalPerMonth;
  }

}

export class MonthlyAnalysis {

  month: number;
  totalAmount: number;

  constructor(month: number, totalAmount: number) {
    this.month = month;
    this.totalAmount = totalAmount;
  }

}
