import {Component, OnInit} from '@angular/core';
import {TransactionAnalysis} from "../transaction-analysis";
import {TransactionService} from "../transaction-service/transaction.service";
import {BudgetService} from "../../budget/budget-service/budget.service";
import {NumberFormatter} from "../../common/number-formatter";
import {Budget} from "../../budget/budget";

@Component({
  selector: 'app-analyze',
  templateUrl: './analyze-screen.component.html',
  styleUrls: ['./analyze-screen.component.scss']
})
export class AnalyzeScreenComponent implements OnInit {

  budgetFilter?: Budget;
  budgetMap: Map<number, Budget> = new Map<number, Budget>();
  budgetList: Budget[] = [];
  data?: TransactionAnalysis;
  displayedColumns: string[] = ['name'].concat(this.range(1, 12).map(n => n.toString())).concat(['amount']);
  numberFormat = NumberFormatter.numberFormat();

  formatCache = new Map<number, string>();

  constructor(private transactionService: TransactionService,
              private budgetService: BudgetService) { }

  ngOnInit(): void {
    this.budgetService.list()
      .subscribe(budgets => {
        budgets._embedded.budgets.forEach(budget => this.budgetMap.set(budget.id!, budget));
        this.budgetList = Array.from(this.budgetMap.values());
        if (this.budgetMap.size > 0) {
          this.budgetFilter = this.budgetList.reduce((a, b) => a.budgetYear < b.budgetYear ? b : a);
        }
        this.fetchAnalysis();
      });
  }

  fetchAnalysis() {
    this.transactionService.analyze(this.budgetFilter!).subscribe(data => {this.data = data});
  }

  format(amount: number): string {
    if (!this.formatCache.has(amount)) {
      this.formatCache.set(amount, !amount ? '' : this.numberFormat.format(amount));
    }
    return this.formatCache.get(amount)!;
  }

  range(from: number, to: number): number[] {
    const range = [];
    for (let i = from; i <= to; i++) {
      range.push(i);
    }
    return range;
  }

  monthShortName(monthValue: number): string {
    switch (monthValue) {
      case 1:
        return `Jan`;
      case 2:
        return `Feb`;
      case 3:
        return `Mar`;
      case 4:
        return `Apr`;
      case 5:
        return `May`;
      case 6:
        return `Jun`;
      case 7:
        return `Jul`;
      case 8:
        return `Aug`;
      case 9:
        return `Sep`;
      case 10:
        return `Oct`;
      case 11:
        return `Nov`;
      case 12:
        return `Dec`;
      default:
        return '';
    }
  }

}
