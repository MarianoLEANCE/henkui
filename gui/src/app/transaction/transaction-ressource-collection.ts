import {Transaction} from './transaction';
import {ResourceCollection} from "../model/resource-collection";
import {Links} from "../model/link";

export class TransactionRessourceCollection extends ResourceCollection<Transactions> {

  constructor(_embedded: Transactions, _links: Links) {
    super(_embedded, _links);
  }

}

export class Transactions {

  transactions: Transaction[];

  constructor(transactions: Transaction[]) {
    this.transactions = transactions;
  }

}
