import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Transaction} from '../transaction';
import {Observable} from 'rxjs';
import {env} from '../../../environments/environment';
import {TransactionRessourceCollection} from '../transaction-ressource-collection';
import {Account} from "../../account/account";
import {Allocation} from "../../allocation/allocation";
import {Budget} from "../../budget/budget";
import {TransactionAnalysis} from "../transaction-analysis";

@Injectable({providedIn: 'root'})
export class TransactionService {

  // TODO use root resource
  url = env.apiUrl + '/api/transactions';

  constructor(private http: HttpClient) { }

  public list(budgetYear: number): Observable<TransactionRessourceCollection> {
    const params = new HttpParams().set('budget-year', budgetYear.toString());
    return this.http.get<TransactionRessourceCollection>(this.url, {params, withCredentials: true});
  }

  public analyze(budget: Budget): Observable<TransactionAnalysis> {
    return this.http.get<TransactionAnalysis>(budget._links!.analyze.href, {withCredentials: true});
  }

  public create(transaction: Transaction, account: Account): Observable<HttpResponse<Transaction>> {
    return this.http.post<Transaction>(account._links!.transactions.href, transaction, {
      observe: 'response',
      withCredentials: true
    });
  }

  public update(transaction: Transaction): Observable<Transaction> {
    return this.http.put<Transaction>(transaction._links!.self.href, transaction, {withCredentials: true});
  }

  delete(transaction: Transaction): Observable<HttpResponse<object>> {
    return this.http.delete(transaction._links!.self.href, {observe: 'response', withCredentials: true});
  }

  allocate(transaction: Transaction, allocation: Allocation) {
    return this.http.put<Transaction>(transaction._links!.allocate.href, allocation, {withCredentials: true});
  }

  deallocate(transaction: Transaction, allocation: Allocation) {
    return this.http.put<Transaction>(transaction._links!.deallocate.href, allocation, {withCredentials: true});

  }
}
