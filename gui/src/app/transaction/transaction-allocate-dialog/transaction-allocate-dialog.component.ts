import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Transaction} from '../transaction';
import {Allocation} from "../../allocation/allocation";
import {TransactionService} from "../transaction-service/transaction.service";
import {BudgetEntry} from "../../budget/budget-entry";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-edit-transaction-dialog',
  templateUrl: './transaction-allocate-dialog.component.html',
  styleUrls: ['./transaction-allocate-dialog.component.scss']
})
export class TransactionAllocateDialogComponent implements OnInit {

  selectedEntry?: BudgetEntry;
  selectedAmount;
  displayedColumns: string[] = ['category', 'subcategory', 'amount', 'actions'];

  constructor(public dialogRef: MatDialogRef<TransactionAllocateDialogComponent>,
              public transactionService: TransactionService,
              public snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public data: {
                transaction: Transaction,
                entries: BudgetEntry[]
              }) {
    this.selectedAmount = data.transaction.amount - data.transaction.allocations
      .map(allocation => allocation.amount)
      .reduce((a, b) => a + b, 0);
  }

  ngOnInit(): void { }

  allocate() {
    const allocation = new Allocation(
      this.selectedEntry?.category!,
      this.selectedEntry?.subcategory!,
      this.selectedAmount);
    this.transactionService.allocate(this.data.transaction, allocation).subscribe(() => {
      this.snackBar.open(`Transaction allocated.`, 'Dismiss', {duration: 1000});
      this.dialogRef.close({action: 'allocated'});
    });
  }

  deallocate(allocation:Allocation) {
    this.transactionService.deallocate(this.data.transaction, allocation).subscribe(() => {
      this.snackBar.open(`Transaction allocated.`, 'Dismiss', {duration: 1000});
      this.dialogRef.close({action: 'allocated'});
    });
  }

  display(category: string, subcategory: string): string {
    return BudgetEntry.display(category, subcategory);
  }

}
