import {Link, Links} from '../model/link';
import {Allocation} from "../allocation/allocation";

export class Transaction {

  accountingDate: string;
  budgetYear: number;
  budgetMonth: number;
  description: string;
  amount: number;
  accountId: number;

  allocations: Allocation[];

  id?: number;
  _links?: TransactionLinks;

  // UI only
  toDelete = false;
  selected = false;

  static of(data: Transaction): Transaction {
    return new Transaction(
      data.accountingDate,
      data.budgetYear,
      data.budgetMonth,
      data.description,
      data.amount,
      data.accountId,
      data.allocations,

      data.id,
      data._links
    );
  }

  constructor(accountingDate: string,
              budgetYear: number,
              budgetMonth: number,
              description: string,
              amount: number,
              accountId: number,
              allocations: Allocation[],
              id?: number,
              _links?: TransactionLinks) {
    this.accountingDate = accountingDate;
    this.budgetYear = budgetYear;
    this.budgetMonth = budgetMonth;
    this.description = description;
    this.amount = amount;
    this.accountId = accountId;
    this.allocations = allocations;

    this.id = id;
    this._links = _links;
  }

}

class TransactionLinks extends Links {

  allocate: Link;
  deallocate: Link;

  constructor(self: Link, allocate: Link, deallocate: Link) {
    super(self);
    this.allocate = allocate;
    this.deallocate = deallocate;
  }
}
