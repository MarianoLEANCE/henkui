import {Component, OnInit} from '@angular/core';
import {Transaction} from '../transaction';
import {MatDialog} from '@angular/material/dialog';
import {Account} from '../../account/account';
import {TransactionService} from '../transaction-service/transaction.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {PageEvent} from '@angular/material/paginator';
import {BreakpointObserver} from '@angular/cdk/layout';
import {AccountService} from '../../account/service/account.service';
import * as moment from "moment";
import {BudgetEntry} from "../../budget/budget-entry";
import {BudgetService} from "../../budget/budget-service/budget.service";
import {Budget} from "../../budget/budget";
import {BudgetEntryService} from "../../budget/budget-service/budget-entry.service";
import {NumberFormatter} from "../../common/number-formatter";
import {TransactionEditDialogComponent} from "../transaction-edit-dialog/transaction-edit-dialog.component";
import {TransactionAllocateDialogComponent} from "../transaction-allocate-dialog/transaction-allocate-dialog.component";
import {Allocation} from "../../allocation/allocation";
import {forkJoin} from "rxjs";

@Component({
  selector: 'app-edit',
  templateUrl: './transaction-screen.component.html',
  styleUrls: ['./transaction-screen.component.scss']
})
export class TransactionScreenComponent implements OnInit {

  loaded = false;
  displayedColumns: string[] = [
    'selected',
    'account',
    'accountingDate',
    'budgetMonth',
    'description',
    'amount',
    'allocations',
    'actions',
  ];

  accountFilter?: Account;
  accountingDateFilter?: Date;
  budgetFilter?: Budget;
  budgetEntryFilter?: BudgetEntry;
  budgetMonthFilter?: number;
  descriptionFilter: string = '';
  unallocatedFilter = false;

  selectedBatchBudgetEntry?: BudgetEntry;

  accounts: Map<number, Account> = new Map<number, Account>();
  budgets: Map<number, Budget> = new Map<number, Budget>();
  budgetEntries: Map<number, BudgetEntry> = new Map<number, BudgetEntry>();

  transactions: Transaction[] = [];
  filteredTransactions: Transaction[] = [];
  currentPage: Transaction[] = [];

  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  numberFormat = NumberFormatter.numberFormat();
  amountFormatter?: (x: number) => string;
  allocationFormatter?: (x: number) => string;

  constructor(private breakpointObserver: BreakpointObserver,
              private dialog: MatDialog,
              private accountService: AccountService,
              private budgetService: BudgetService,
              private budgetEntryService: BudgetEntryService,
              private transactionService: TransactionService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchAccounts();
    this.fetchBudgets();
    this.allocationFormatter = amount => this.numberFormat.format(amount);
  }

  private fetchAccounts() {
    this.accountService.list()
      .subscribe(accounts => accounts._embedded.accounts
        .forEach(account => this.accounts.set(account.id!, account)));
  }

  private fetchBudgets() {
    this.budgetService.list()
      .subscribe(budgets => {
        budgets._embedded.budgets.forEach(budget => this.budgets.set(budget.id!, budget));
        if (this.budgets.size > 0) {
          this.budgetFilter = this.budgetList().reduce((a, b) => a.budgetYear < b.budgetYear ? b : a);
        }
        this.fetchTransactions();
      });
  }

  fetchTransactions() {
    this.loaded = false;
    if (!this.budgetFilter) {
      return;
    }

    this.budgetEntryService.list(this.budgetFilter)
      .subscribe(entries => entries._embedded.budgetEntries.forEach(entry => this.budgetEntries.set(entry.id!, entry)));

    this.transactionService.list(this.budgetFilter.budgetYear)
      .subscribe(transactions => {
        this.transactions = transactions._embedded.transactions;
        this.filterTransactions();
        this.loaded = true;
      });
  }

  display(category: string, subcategory: string): string {
    return BudgetEntry.display(category, subcategory);
  }

  editDialog(transaction: Transaction): void {
    this.dialog.open(TransactionEditDialogComponent, {data: transaction})
      .afterClosed().subscribe(event => {
      if (event?.action === 'updated') {
        this.fetchTransactions();
      }
    });
  }

  allocateDialog(transaction: Transaction): void {
    this.dialog.open(TransactionAllocateDialogComponent, {
      width: '512x',
      data: {
        transaction: transaction,
        entries: this.budgetEntriesList()
      }
    }).afterClosed().subscribe(event => {
      if (event?.action === 'allocated') {
        this.fetchTransactions();
      }
    });
  }

  confirmDelete(transaction: Transaction) {
    this.snackBar.open(`Confirm delete transaction.`, `Delete`, {duration: 5000})
      .onAction().subscribe(() => this.delete(transaction));
  }

  //region delete

  delete(transaction: Transaction) {
    this.transactionService.delete(transaction).subscribe(() => {
      this.snackBar.open('Transaction deleted.', 'Dismiss', {duration: 5000});
      this.fetchTransactions();
    })
  }

  filterTransactions() {
    this.deselectAll();
    this.filteredTransactions = this.transactions
      .filter(t => (!this.accountFilter || t.accountId === this.accountFilter.id))
      .filter(t => (!this.accountingDateFilter || this.equals(t.accountingDate, this.accountingDateFilter)))
      .filter(t => (!this.budgetMonthFilter || t.budgetMonth === this.budgetMonthFilter))
      .filter(t => (!this.descriptionFilter || this.includesIgnoreCase(t.description)))
      .filter(t => (!this.budgetEntryFilter || this.containsBudgetEntry(t)))
      .filter(t => (!this.unallocatedFilter || this.isUnallocated(t)))
    this.page();
  }

  equals(s: string, d: Date) {
    return s === moment(d).format("YYYY-MM-DD");
  }

  includesIgnoreCase(text: string): boolean {
    return text.toLowerCase().includes(this.descriptionFilter.toLowerCase());
  }

  containsBudgetEntry(transaction: Transaction): boolean {
    return transaction.allocations.filter(allocation =>
      allocation.category === this.budgetEntryFilter!.category
      && allocation.subcategory === this.budgetEntryFilter!.subcategory).length > 0;
  }

  isUnallocated(transaction: Transaction): boolean {
    return transaction.allocations
      .map(transaction => transaction.amount)
      .reduce((acc, cur) => acc + cur, 0) !== transaction.amount;
  }

  page(event ?: PageEvent) {
    this.deselectAll();
    if (event) {
      this.pageSize = event.pageSize;
    }
    const index = event ? event.pageIndex : 0;
    const pageSize = this.pageSize;
    const start = index * pageSize;
    const end = start + pageSize;
    this.currentPage = this.filteredTransactions.slice(start, end);
    const numberFormat = NumberFormatter.numberFormat();
    const formatter = (a: number[]) => (n: number) => NumberFormatter
      .format(numberFormat, NumberFormatter.maxLength(numberFormat, a), n);
    this.amountFormatter = formatter(this.currentPage.map(entry => entry.amount));
  }

  selectAll() {
    this.transactions.forEach(t => t.selected = false);
    this.currentPage.forEach(t => t.selected = true);
  }

  deselectAll() {
    this.filteredTransactions.forEach(t => t.selected = false);
  }

  accountList(): Account[] {
    return Array.from(this.accounts.values());
  }

  budgetList(): Budget[] {
    return Array.from(this.budgets.values());
  }

  budgetEntriesList(): BudgetEntry[] {
    return Array.from(this.budgetEntries.values());
  }

  // region batch

  transactionsSelected(): boolean {
    for (const i in this.filteredTransactions) {
      if (this.filteredTransactions[i].selected) {
        return true;
      }
    }
    return false;
  }

  batchUpdate() {
    forkJoin(this.filteredTransactions
      .filter(transaction => transaction.selected)
      .map(transaction => {
        const allocation = new Allocation(
          this.selectedBatchBudgetEntry?.category!,
          this.selectedBatchBudgetEntry?.subcategory!,
          transaction.amount);
        return this.transactionService.allocate(transaction, allocation);
      })
    ).subscribe(() => {
      this.snackBar.open(`Transactions allocated.`, 'Dismiss', {duration: 1000});
      this.fetchTransactions()
    });
  }

  // endregion

}
