export class SpendingRate {

  cumulativeExpenses: SpendingRateSnapshot[];
  category: string;
  subcategory: string;
  amount: number;

  constructor(cumulativeExpenses: SpendingRateSnapshot[], category: string, subcategory: string, amount: number) {
    this.cumulativeExpenses = cumulativeExpenses;
    this.category = category;
    this.subcategory = subcategory;
    this.amount = amount;
  }

}

export class SpendingRateSnapshot {

  accountingDate: string;
  actualAmount: number;
  maxPerYear: number;
  maxPerDay: number;

  constructor(accountingDate: string, actualAmount: number, maxPerYear: number, maxPerDay: number) {
    this.accountingDate = accountingDate;
    this.actualAmount = actualAmount;
    this.maxPerYear = maxPerYear;
    this.maxPerDay = maxPerDay;
  }

}
