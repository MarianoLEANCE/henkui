import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BudgetEntryService} from "../budget-service/budget-entry.service";
import {BudgetEntry} from "../budget-entry";
import {LineChartData} from "../../common/charts/line-chart-data";
import {SpendingRate} from "../spending-rate";

@Component({
  selector: 'app-budget-entry-spending-rate-dialog',
  templateUrl: './budget-entry-spending-rate-dialog.component.html',
  styleUrls: ['./budget-entry-spending-rate-dialog.component.scss']
})
export class BudgetEntrySpendingRateDialogComponent implements OnInit {

  category?: string;
  //https://swimlane.github.io/ngx-charts/#/ngx-charts/line-chart
  data?: LineChartData[];

  constructor(private dialogRef: MatDialogRef<BudgetEntrySpendingRateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public entry: BudgetEntry,
              private budgetEntryService: BudgetEntryService) {
    console.log(entry)
  }

  ngOnInit(): void {
    this.budgetEntryService.spendingRate(this.entry)
      .subscribe(spendingRate => {
        this.category = BudgetEntry.display(spendingRate.category, spendingRate.subcategory);
        this.data = [
          this.spendingRateData(spendingRate),
          this.maxPerDay(spendingRate),
          this.maxPerYear(spendingRate)
        ];
        console.log(this.data);
      });
  }

  spendingRateData(spendingRate: SpendingRate): LineChartData {
    const name = 'actual';
    const series = spendingRate.cumulativeExpenses
      .filter(snapshot => snapshot.actualAmount != null)
      .map((snapshot) => {
        return {
          name: snapshot.accountingDate,
          value: snapshot.actualAmount
        }
      });
    return new LineChartData(name, series);
  }

  maxPerDay(spendingRate: SpendingRate): LineChartData {
    const name = 'per day';
    const series = spendingRate.cumulativeExpenses
      .filter(snapshot => snapshot.actualAmount != null)
      .map(snapshot => {
        return {
          name: snapshot.accountingDate,
          value: snapshot.maxPerDay
        }
      });
    return new LineChartData(name, series);
  }

  maxPerYear(spendingRate: SpendingRate): LineChartData {
    const name = 'per year';
    const series = spendingRate.cumulativeExpenses
      .filter(snapshot => snapshot.actualAmount != null)
      .map((snapshot) => {
        return {
          name: snapshot.accountingDate,
          value: snapshot.maxPerYear
        }
      });
    return new LineChartData(name, series);
  }

  isExpense(): boolean {
    return this.entry.amount < 0;
  }

  isEarning(): boolean {
    return this.entry.amount > 0;
  }
}
