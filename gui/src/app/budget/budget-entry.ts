import {Link, Links} from "../model/link";

export class BudgetEntry {

  amount: number;
  actual: number;
  category: string;
  subcategory: string;

  id?: number;
  _links?: BudgetEntryLinks;

  static of(data: BudgetEntry): BudgetEntry {
    return new BudgetEntry(
      data.amount,
      data.actual,
      data.category,
      data.subcategory,

      data.id,
      data._links
    );
  }

  constructor(amount: number, actual: number, category: string, subcategory: string,
              id?: number, links?: BudgetEntryLinks) {
    this.amount = amount;
    this.actual = actual;
    this.category = category;
    this.subcategory = subcategory;

    this.id = id;
    this._links = links;
  }

  static display(category: string, subcategory: string): string {
    return subcategory === '' ? category : `${category}/${subcategory}`;
  }

  static isExpense(entry: BudgetEntry) {
    return entry.amount < 0;
  }

  static isEarning(entry: BudgetEntry) {
    return entry.amount > 0;
  }

}

export class BudgetEntryLinks extends Links {

  spendingRate: Link;

  constructor(self: Link, spendingRate: Link) {
    super(self);
    this.spendingRate = spendingRate;
  }

}
