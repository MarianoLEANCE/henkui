import {ResourceCollection} from "../model/resource-collection";
import {Links} from "../model/link";
import {BudgetEntry} from "./budget-entry";

export class BudgetEntryRessourceCollection extends ResourceCollection<BudgetEntries> {

  constructor(_embedded: BudgetEntries, _links: Links) {
    super(_embedded, _links);
  }

}

export class BudgetEntries {

  budgetEntries: BudgetEntry[];

  constructor(entries: BudgetEntry[]) {
    this.budgetEntries = entries;
  }

}
