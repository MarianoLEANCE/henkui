import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {Budget} from "../budget";
import {BudgetEntryRessourceCollection} from "../budget-entry-ressource-collection";
import {BudgetEntry} from "../budget-entry";
import {SpendingRate} from "../spending-rate";

@Injectable({
  providedIn: 'root'
})
export class BudgetEntryService {

  constructor(private http: HttpClient) { }

  public list(budget: Budget): Observable<BudgetEntryRessourceCollection> {
    return this.http.get<BudgetEntryRessourceCollection>(budget._links!.entries.href, {withCredentials: true});
  }

  public update(entry: BudgetEntry): Observable<BudgetEntry> {
    return this.http.put<BudgetEntry>(entry._links!.self.href, entry, {withCredentials: true});
  }

  public create(budget: Budget, entry: BudgetEntry): Observable<BudgetEntry> {
    return this.http.post<BudgetEntry>(budget._links!.entries.href, entry, {withCredentials: true});
  }

  public delete(entry: BudgetEntry): Observable<HttpResponse<object>> {
    return this.http.delete(entry._links!.self.href, {observe: 'response', withCredentials: true});
  }

  public spendingRate(entry: BudgetEntry): Observable<SpendingRate> {
    return this.http.get<SpendingRate>(entry._links!.spendingRate.href, {withCredentials: true});
  }

}
