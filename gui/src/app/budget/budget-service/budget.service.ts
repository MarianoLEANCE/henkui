import {Injectable} from '@angular/core';
import {env} from "../../../environments/environment";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {BudgetRessourceCollection} from "../budget-ressource-collection";
import {Budget} from "../budget";

@Injectable({providedIn: 'root'})
export class BudgetService {

  // TODO use root resource
  url = env.apiUrl + '/api/budgets';

  constructor(private http: HttpClient) { }

  public list(): Observable<BudgetRessourceCollection> {
    return this.http.get<BudgetRessourceCollection>(this.url, {withCredentials: true});
  }

  public create(budget: Budget): Observable<HttpResponse<Budget>> {
    return this.http.post<Budget>(this.url, budget, {observe: 'response', withCredentials: true});
  }

  // public update(budget: Budget) {
  //   return this.http.put<Budget>(budget._links!.self.href, budget, {withCredentials: true});
  // }
  //
  // delete(budget: Budget): Observable<HttpResponse<object>> {
  //   return this.http.delete(budget._links!.self.href, {observe: 'response', withCredentials: true});
  // }

}
