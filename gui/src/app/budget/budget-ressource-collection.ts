import {ResourceCollection} from "../model/resource-collection";
import {Links} from "../model/link";
import {Budget} from "./budget";

export class BudgetRessourceCollection extends ResourceCollection<Budgets> {

  constructor(_embedded: Budgets, _links: Links) {
    super(_embedded, _links);
  }

}

export class Budgets {

  budgets: Budget[];

  constructor(budgets: Budget[]) {
    this.budgets = budgets;
  }

}
