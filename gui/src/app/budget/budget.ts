import {Link, Links} from "../model/link";

export class Budget {

  budgetYear: number;

  id?: number;
  _links?: BudgetLinks;

  static of(data: Budget): Budget {
    return new Budget(
      data.budgetYear,

      data.id,
      data._links
    );
  }

  constructor(budgetYear: number,
              id?: number,
              _links?: BudgetLinks) {
    this.budgetYear = budgetYear;
    this.id = id;
    this._links = _links;
  }

}

export class BudgetLinks extends Links {

  entries: Link;
  analyze: Link;

  constructor(self: Link, entries: Link, analyze: Link) {
    super(self);
    this.entries = entries;
    this.analyze = analyze;
  }

}
