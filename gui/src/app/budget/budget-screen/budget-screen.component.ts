import {Component, OnInit} from '@angular/core';
import {Budget} from "../budget";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {BudgetService} from "../budget-service/budget.service";
import {BudgetEntry} from "../budget-entry";
import {BudgetEntryService} from "../budget-service/budget-entry.service";
import {env} from "../../../environments/environment";
import {NumberFormatter} from "../../common/number-formatter";
import {BudgetEditDialogComponent} from "../budget-edit-dialog/budget-edit-dialog.component";
import {Subject} from "rxjs";
import {
  BudgetEntrySpendingRateDialogComponent
} from "../budget-entry-spending-rate-dialog/budget-entry-spending-rate-dialog.component";

@Component({
  selector: 'app-budget',
  templateUrl: './budget-screen.component.html',
  styleUrls: ['./budget-screen.component.scss']
})
export class BudgetScreenComponent implements OnInit {

  budgetFilter?: Budget;
  budgetMap: Map<number, Budget> = new Map<number, Budget>();
  budgetList: Budget[] = [];
  budgetEntries: BudgetEntry[] = [];
  yearProgress: number = 0;

  displayedColumns: string[] = ['progress', 'category', 'subcategory', 'status', 'actual', 'amount', 'leftPerYear',
    'actualPerMonth', 'budgetPerMonth', 'actions'];

  loaded = false;

  newEntry: BudgetEntry = new BudgetEntry(0, 0, '', '');

  update$: Subject<any> = new Subject();
  yearProgressData: { name: string, value: number }[] = [];
  balanceData: { name: string, value: number }[] = [];
  earningsData: { name: string, value: number }[] = [];
  expensesData: { name: string, value: number }[] = [];

  actualFormatter?: (x: number) => string;
  amountFormatter?: (x: number) => string;
  leftFormatter?: (x: number) => string;
  actualPerMonthFormatter?: (x: number) => string;
  budgetPerMonthFormatter?: (x: number) => string;

  constructor(private dialog: MatDialog,
              private budgetService: BudgetService,
              private budgetEntryService: BudgetEntryService,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetchBudgets();
  }

  fetchBudgets() {
    this.budgetService.list()
      .subscribe(budgets => {
        budgets._embedded.budgets.forEach(budget => this.budgetMap.set(budget.id!, budget));
        this.budgetList = Array.from(this.budgetMap.values());
        if (this.budgetMap.size > 0) {
          this.budgetFilter = this.budgetList.reduce((a, b) => a.budgetYear < b.budgetYear ? b : a);
        }
        this.fetchBudgetEntries();

        const now = new Date();
        if (this.budgetFilter!.budgetYear < now.getFullYear()) {
          this.yearProgress = 1;
        } else {
          this.yearProgress = Math.abs(this.dayOfYear(now) / this.lengthOfYear(now));
        }

      });
  }

  fetchBudgetEntries() {
    this.loaded = false;
    this.budgetEntryService.list(this.budgetFilter!)
      .subscribe(cm => {
        this.budgetEntries = cm._embedded.budgetEntries;
        const numberFormat = NumberFormatter.numberFormat();
        const formatter = (a: number[]) => (n: number) => NumberFormatter
          .format(numberFormat, NumberFormatter.maxLength(numberFormat, a), n);

        this.actualFormatter = formatter(this.budgetEntries.map(entry => entry.actual));
        this.amountFormatter = formatter(this.budgetEntries.map(entry => entry.amount));
        this.leftFormatter = formatter(this.budgetEntries.map(entry => entry.amount - entry.actual));
        this.actualPerMonthFormatter = formatter(this.budgetEntries.map(entry => this.actualPerMonth(entry)));
        this.budgetPerMonthFormatter = formatter(this.budgetEntries.map(entry => this.budgetPerMonth(entry)));

        this.balanceData = [
          {name: 'earnings', value: this.budgetedEarnings()},
          {name: 'expenses', value: this.budgetedExpenses()}
        ];
        this.yearProgressData = [{name: 'elapsed', value: this.yearProgress * 100}];
        this.earningsData = this.earningsChartData();
        this.expensesData = this.expensesChartData();
        this.loaded = true;
        this.updateChart();
      });
  }

  updateChart() {
    this.update$.next(true);
  }

  earningsChartData(): { name: string, value: number }[] {
    const items = new Map<string, number>();

    this.budgetEntries
      .filter(entry => entry.amount > 0)
      .forEach(entry => items.set(entry.category, entry.amount + (items.get(entry.category) || 0)));

    let a: { name: string, value: number }[] = [];
    items.forEach((value, key) => a.push({name: key, value: value}));
    return a;
  }

  expensesChartData(): { name: string, value: number }[] {
    const items = new Map<string, number>();

    this.budgetEntries
      .filter(entry => entry.amount < 0)
      .forEach(entry => items.set(entry.category, -entry.amount + (items.get(entry.category) || 0)));

    let a: { name: string, value: number }[] = [];
    items.forEach((value, key) => a.push({name: key, value: value}));
    return a;
  }

  dayOfYear(date: Date) {
    return (Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())
      - Date.UTC(date.getFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
  }

  lengthOfYear(date: Date): number {
    return this.isLeapYear(date.getFullYear()) ? 366 : 365;
  }

  isLeapYear(year: number): boolean {
    return year % 4 === 0 && year % 100 !== 0 && year % 400 === 0;
  }

  progress(element: BudgetEntry): number {
    return Math.abs(element.actual / element.amount) * 100;
  }

  status(entry: BudgetEntry): { color: string, message: string } {

    if (BudgetEntry.isExpense(entry) && entry.amount > 0) {
      return {color: 'warn', message: `Expense instead of earning: use a negative amount for expenses.`};
    }

    if (BudgetEntry.isEarning(entry) && entry.amount < 0) {
      return {color: 'warn', message: `Earning instead of expense: use a positive amount for earnings.`};
    }

    if (BudgetEntry.isExpense(entry) && entry.actual < entry.amount) {
      const overspent = Math.abs(entry.actual - entry.amount);
      const amount = this.formatMonetaryAmount(overspent);
      return {color: 'warn', message: `You spent ${amount} more than budgeted.`};
    }

    if (BudgetEntry.isEarning(entry) && entry.actual > entry.amount) {
      const overearned = entry.actual - entry.amount;
      const amount = this.formatMonetaryAmount(overearned);
      return {color: 'warn', message: `You earned ${amount} more than budgeted.`};
    }

    // within range
    if (BudgetEntry.isEarning(entry)) {
      return {color: 'primary', message: ''};
    } else {
      return {color: 'accent', message: ''};
    }
  }

  actualPerMonth(entry: BudgetEntry): number {
    return entry.actual / this.yearProgress / 12;
  }

  budgetPerMonth(entry: BudgetEntry): number {
    return entry.amount / 12;
  }

  formatMonetaryAmount(amount: number): string {
    return new Intl.NumberFormat(`en-US`, {maximumFractionDigits: env.currency.fractionDigits})
      .format(amount) + ' ' + env.currency.code;
  }

  budgetedEarnings(): number {
    return this.budgetEntries
      .filter(entry => entry.amount > 0)
      .map(entry => entry.amount)
      .reduce((a, b) => a + b, 0);
  }

  budgetedExpenses(): number {
    return -this.budgetEntries
      .filter(entry => entry.amount < 0)
      .map(entry => entry.amount)
      .reduce((a, b) => a + b, 0);
  }

  balance(): number {
    return this.budgetEntries
      .map(entry => entry.amount)
      .reduce((a, b) => a + b, 0);
  }

  delete(entry: BudgetEntry) {
    this.snackBar.open(`Confirm delete budget entry ${this.description(entry)}.`, `Delete`,
      {duration: 5000})
      .onAction()
      .subscribe(() => {
        this.budgetEntryService.delete(entry).subscribe(() => {
          this.fetchBudgetEntries();
          this.snackBar.open(`Budget entry deleted.`, 'Delete', {duration: 1000});
        });
      });
  }

  description(budgetAnalysis: BudgetEntry): string {
    return budgetAnalysis.category + (budgetAnalysis.subcategory ? '/' + budgetAnalysis.subcategory : '');
  }

  createNew() {
    this.newEntry.category = this.newEntry.category || '';
    this.newEntry.subcategory = this.newEntry.subcategory || '';
    this.budgetEntryService.create(this.budgetFilter!, this.newEntry).subscribe(() => {
      this.snackBar.open(`Budget entry created.`, 'Dismiss', {duration: 1000});
      this.fetchBudgetEntries();
    }, () => {
      this.snackBar.open(`Error: could not create budget entry.`, 'Dismiss', {duration: 1000});
      this.fetchBudgetEntries();
    });
  }

  edit(entry: BudgetEntry) {
    this.dialog.open(BudgetEditDialogComponent, {
      width: '408px',
      data: entry
    }).afterClosed().subscribe(() => {
      this.fetchBudgetEntries();
      this.snackBar.open(`Budget entry updated.`, 'Create', {duration: 1000});
    });
  }

  spendingRate(budgetEntry: BudgetEntry) {
    this.dialog.open(BudgetEntrySpendingRateDialogComponent, {
      width: '1230px',
      height: '800px',
      data: budgetEntry
    });
  }

  digitsInfo(): string {
    return `1.${env.currency.fractionDigits}-${env.currency.fractionDigits}`;
  }

}
