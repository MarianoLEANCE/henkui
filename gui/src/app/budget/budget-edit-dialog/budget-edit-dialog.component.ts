import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {BudgetEntry} from "../budget-entry";
import {BudgetEntryService} from "../budget-service/budget-entry.service";
import {NumberFormatter} from "../../common/number-formatter";

@Component({
  selector: 'app-budget-edit-dialog',
  templateUrl: './budget-edit-dialog.component.html',
  styleUrls: ['./budget-edit-dialog.component.scss']
})
export class BudgetEditDialogComponent implements OnInit {

  public copy: BudgetEntry;

  constructor(private dialogRef: MatDialogRef<BudgetEditDialogComponent>,
              private snackBar: MatSnackBar,
              private service: BudgetEntryService,
              @Inject(MAT_DIALOG_DATA) private entry: BudgetEntry) {
    this.copy = BudgetEntry.of(entry);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): BudgetEntry {
    this.update();
    this.service.update(this.entry).subscribe(() => {
      this.snackBar.open(`Budget entry updated.`, 'Update', {duration: 1000});
    });
    this.dialogRef.close('updated');
    return this.entry;
  }

  ngOnInit(): void { }

  update(): void {
    this.entry.amount = this.copy.amount;
    this.entry.category = this.copy.category;
    this.entry.subcategory = this.copy.subcategory;
  }

  abs(n: number): number {
    return Math.abs(n);
  }

  format(n: number): string {
    return NumberFormatter.format(NumberFormatter.numberFormat(), 0, n);
  }

}
